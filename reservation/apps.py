# ./reservation/apps.py
from django.apps import AppConfig


class ReservationConfig(AppConfig):
    name = 'reservation'
