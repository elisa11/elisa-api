# ./subjects/apps.py
from django.apps import AppConfig


class SubjectsConfig(AppConfig):
    name = 'subjects'
