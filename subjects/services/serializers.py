from rest_framework import serializers

from elisa.utils.LeveledSerializer import LeveledModelSerializer, \
    LeveledNestedField
from users.services.serializers import UserSerializer
from building.services.serializers import BuildingSerializer
from subjects.models import Subject, SubjectGroup, SubjectUserCollision, SubjectUserRole

class CrossSubjectCollisionSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubjectUserCollision
        fields = ['subject_a', 'subject_b', 'collision_count', 'subject_a_code', 'subject_b_code']


class SubjectSerializer(LeveledModelSerializer):
    building = LeveledNestedField(BuildingSerializer, read_only=True)
    
    class Meta:
        model = Subject
        fields = ['id', 'name', 'code', 'nominal_semester', 'building']


class SubjectUserRoleSerializer(LeveledModelSerializer):
    class Meta:
        model = SubjectUserRole
        fields = ['user', 'subject', 'name']


class SubjectGroupSerializer(LeveledModelSerializer):
    class Meta:
        model = SubjectGroup
        fields = ['id', 'subject', 'name']
