# ./subjects/services/filters.py
import django_filters
from subjects.models import Subject, SubjectGroup, SubjectUserCollision, \
    SubjectUserRole

class SubjectFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='iregex')
    code = django_filters.CharFilter(lookup_expr='iregex')
    nominal_semester = django_filters.NumberFilter()

    class Meta:
        model = Subject
        fields = ['name', 'code', 'nominal_semester']

class SubjectUserRoleFilter(django_filters.FilterSet):
    subject = django_filters.NumberFilter()
    user = django_filters.NumberFilter()
    name = django_filters.CharFilter(lookup_expr='iregex')

    class Meta:
        model = SubjectUserRole
        fields = ['subject', 'user', 'name']

class SubjectUserCollisionFilter(django_filters.FilterSet):
    subject_a = django_filters.NumberFilter()
    subject_b = django_filters.NumberFilter()

    class Meta:
        model = SubjectUserCollision
        fields = ['subject_a', 'subject_b']

class SubjectGroupFilter(django_filters.FilterSet):
    subject = django_filters.NumberFilter()
    subject__code = django_filters.CharFilter(lookup_expr='iregex')
    name = django_filters.CharFilter(lookup_expr='iregex')
    subject__nominal_semester = django_filters.NumberFilter()
    class Meta:
        model = SubjectGroup
        fields = ['subject', 'name', 'subject__code', 'subject__nominal_semester']
        