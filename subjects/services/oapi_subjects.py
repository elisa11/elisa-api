# ./subjects/services/oapi_subjects.py
from drf_spectacular.utils import OpenApiExample


class oapi_SubjectStudentCount:
    examples = [
        OpenApiExample(
            name="Specific Subject",
            value=[
                {"101": 25}
            ],
            description="Response when a specific subject_id is provided. In this case, subject 101 has 25 students."
        ),
        OpenApiExample(
            name="All Subjects",
            value=[
                {"101": 25},
                {"102": 30},
                {"103": 45}
            ],
            description="Response when no subject_id is provided. It returns the count of students for all subjects."
        )
    ]
