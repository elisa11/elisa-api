# ./subjects/urls.py
from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import SubjectGroupViewSet, SubjectUserCollisionViewSet, SubjectUserRoleViewSet, SubjectViewSet



router = DefaultRouter()
router.register(r'subjects', SubjectViewSet)
router.register(r'subject-user-roles', SubjectUserRoleViewSet)
router.register(r'subject-user-collisions', SubjectUserCollisionViewSet)
router.register(r'subject-groups', SubjectGroupViewSet)

urlpatterns = router.urls