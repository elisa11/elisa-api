# ./subjects/models.py
from django.db import models
from building.models import Building
from users.models import User


class Subject(models.Model):
    code = models.CharField(max_length=50, unique=True)
    name = models.CharField(max_length=255, unique=False)
    building = models.ForeignKey(Building, on_delete=models.SET_NULL, null=True)
    nominal_semester = models.IntegerField(blank=True, null=True)

    class Meta:
        indexes = [
            models.Index(fields=['code']),
            models.Index(fields=['name']),
            models.Index(fields=['nominal_semester']),
        ]
        unique_together = [('code', 'name'),
                       ('code', 'nominal_semester'),]

    def __str__(self):
        return self.name


class SubjectGroup(models.Model):
    id = models.AutoField(primary_key=True)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, unique=False)

    def __str__(self):
        return f"{self.subject.code} - {self.name}"
    class Meta:
        unique_together = [('subject', 'name'),]


class SubjectUserRole(models.Model):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.subject.code} - {self.user.username} - {self.name}"
    class Meta:
        unique_together = [('user', 'subject', 'name'),]

class SubjectUserCollision(models.Model):
    subject_a = models.ForeignKey(Subject, related_name='collisions_as_subject_a', on_delete=models.DO_NOTHING, null=True, db_column='subject_a')
    subject_b = models.ForeignKey(Subject, related_name='collisions_as_subject_b', on_delete=models.DO_NOTHING, null=True, db_column='subject_b')
    collision_count = models.IntegerField()
    subject_a_code = models.CharField(max_length=50, null=True)
    subject_b_code = models.CharField(max_length=50, null=True)

    class Meta:
        managed = False  # dont manage DB view
        db_table = 'subject_user_collisions'
        unique_together = [('subject_a', 'subject_b'),]
    def __str__(self):
        return f"Cross Subject Collision {self.subject_a.code if self.subject_a else 'None'}-{self.subject_b.code if self.subject_b else 'None'}: {self.collision_count}"
