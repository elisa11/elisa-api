# ./subjects/views.py
from django.db import connection, transaction
from django.db.models import Count
from drf_spectacular.utils import OpenApiParameter, OpenApiResponse, extend_schema

from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

from elisa.utils.LeveledSerializer import LeveledSerializerMixin
from elisa.utils.serializers import Err_serializer, Ok_serializer, err_response, ok_response
from elisa.utils.global_oapi import term_schema_view, generic_django_ops_docs

from .models import Subject, SubjectGroup, SubjectUserCollision, SubjectUserRole
from subjects.services.serializers import CrossSubjectCollisionSerializer, \
    SubjectGroupSerializer, SubjectUserRoleSerializer

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from subjects.services.serializers import SubjectSerializer
from subjects.services.filters import SubjectFilter, SubjectUserRoleFilter, SubjectUserCollisionFilter, SubjectGroupFilter
from subjects.services.oapi_subjects import oapi_SubjectStudentCount
from rest_framework import mixins, viewsets


@term_schema_view()
@generic_django_ops_docs
@extend_schema(tags=["Subjects"])
class SubjectViewSet(LeveledSerializerMixin, viewsets.ModelViewSet):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = SubjectFilter
    ordering_fields = ['id', 'name', 'code', 'nominal_semester']
    ordering = ['id']

    @extend_schema(
        summary="Retrieve the count of students for a subject",
        description="Returns the count of students for a specific subject or for all subjects if no subject_id is provided. Invalid subject_id results in ok with count 0.",
        parameters=[
            OpenApiParameter(
                name='subject_id',
                description='ID of the subject to filter by',
                required=False,
                type=int
            ),
        ],
        responses= {
            200: OpenApiResponse(
            response={
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "subject_id": {"type": "integer"},
                        "student_count": {"type": "integer"}
                    }
                }
            },
            description="List of subjects with student counts.",
            examples= oapi_SubjectStudentCount.examples
            )
        }
    )
    @action(detail=False, methods=['get'])
    def get_student_count(self, request):
        # Get subject_id from the query params
        subject_id = request.query_params.get('id', None)

        if subject_id:
            # If subject_id is provided, get student count for that specific subject
            student_count = SubjectUserRole.objects.filter(subject_id=subject_id, name="student").count()
            result = [{"subject_id": int(subject_id), "student_count": student_count}]
        else:
            # If no subject_id is provided, get student counts for all subjects
            student_counts = (
                SubjectUserRole.objects
                .filter(name="student")
                .values('subject')
                .annotate(count=Count('subject'))
            )
            result = [
                {"subject_id": item['subject'], "student_count": item['count']} for item in student_counts
            ]

        return Response(result, status=status.HTTP_200_OK)

@term_schema_view()
@generic_django_ops_docs
@extend_schema(tags=["Subject User Roles"])
class SubjectUserRoleViewSet(LeveledSerializerMixin, viewsets.ModelViewSet):
    queryset = SubjectUserRole.objects.all()
    serializer_class = SubjectUserRoleSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = SubjectUserRoleFilter
    ordering_fields = ['user', 'subject', 'name']
    ordering = ['user']

@term_schema_view()
@generic_django_ops_docs
@extend_schema(tags=["Subject User Collisions"])
class SubjectUserCollisionViewSet(mixins.ListModelMixin,viewsets.GenericViewSet):
    queryset = SubjectUserCollision.objects.all()
    serializer_class = CrossSubjectCollisionSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = SubjectUserCollisionFilter
    ordering_fields = ['subject_a', 'subject_b']
    ordering = ['subject_a']

@term_schema_view()
@generic_django_ops_docs
@extend_schema(tags=["Subject Groups"])
class SubjectGroupViewSet(LeveledSerializerMixin, viewsets.ModelViewSet):
    queryset = SubjectGroup.objects.all()
    serializer_class = SubjectGroupSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = SubjectGroupFilter
    ordering_fields = ['subject', 'name']
    ordering = ['subject']


