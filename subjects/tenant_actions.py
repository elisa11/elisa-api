from django.db import connection
from django_tenants.utils import schema_context
from tenantManager.tenant_actions import CustomTenantActions
from subjects.models import SubjectUserCollision, SubjectUserRole, Subject

class SubjectCollisionAction(CustomTenantActions):
    def post_create(self, schema_name: str) -> None:
        """Create view after schema creation"""
        with schema_context(schema_name), connection.cursor() as cursor:
            cursor.execute(f"""
                CREATE VIEW {SubjectUserCollision._meta.db_table} AS
                SELECT 
                    row_number() OVER () AS id,
                    LEAST(a.subject_id, b.subject_id) AS subject_a,
                    GREATEST(a.subject_id, b.subject_id) AS subject_b,
                    sa.code AS subject_a_code,
                    sb.code AS subject_b_code,
                    COUNT(DISTINCT a.user_id) AS collision_count
                FROM {SubjectUserRole._meta.db_table} a
                JOIN {SubjectUserRole._meta.db_table} b ON a.user_id = b.user_id
                JOIN {Subject._meta.db_table} sa ON sa.id = LEAST(a.subject_id, b.subject_id)
                JOIN {Subject._meta.db_table} sb ON sb.id = GREATEST(a.subject_id, b.subject_id)
                WHERE a.subject_id != b.subject_id
                AND a.name = 'student'
                AND b.name = 'student'
                GROUP BY subject_a, subject_b, sa.code, sb.code;
            """)