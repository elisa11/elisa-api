# ./subjects/tests/test_subjects_collisions.py
from django.urls import reverse

from rest_framework import status

from subjects.models import Subject, SubjectUserRole, SubjectUserCollision
from subjects.services.serializers import SubjectSerializer
from users.models import User
from users.services.serializers import UserSerializer, SubjectUserRoleSerializer
import test.helpers


class SubjectCollisionsTestCase(test.helpers.TestBase):
    def setUp(self):
        self._subject1 = Subject.objects.create(code="T-Test1", name="Test Subject1", nominal_semester=1)
        self._subject2 = Subject.objects.create(code="T-Test2", name="Test Subject2", nominal_semester=2)
        self._subject3 = Subject.objects.create(code="T-Test3", name="Test Subject3", nominal_semester=3)

        self._subject1_json = SubjectSerializer(self._subject1).data
        self._subject2_json = SubjectSerializer(self._subject2).data
        self._subject3_json = SubjectSerializer(self._subject3).data

        self._student1 = User.objects.create(username="testuser1", full_name="Test User1")
        self._student2 = User.objects.create(username="testuser2", full_name="Test User2")

        self._student1_json = UserSerializer(self._student1).data
        self._student2_json = UserSerializer(self._student2).data

        self._role1 = SubjectUserRole.objects.create(user=self._student1, subject=self._subject1, name="student")
        self._role2 = SubjectUserRole.objects.create(user=self._student2, subject=self._subject1, name="student")
        self._role3 = SubjectUserRole.objects.create(user=self._student1, subject=self._subject2, name="student")
        self._role4 = SubjectUserRole.objects.create(user=self._student2, subject=self._subject2, name="student")
        self._role5 = SubjectUserRole.objects.create(user=self._student1, subject=self._subject3, name="teacher")
        self._role6 = SubjectUserRole.objects.create(user=self._student2, subject=self._subject3, name="student")

        self._role1_json = SubjectUserRoleSerializer(self._role1).data
        self._role2_json = SubjectUserRoleSerializer(self._role2).data
        self._role3_json = SubjectUserRoleSerializer(self._role3).data
        self._role4_json = SubjectUserRoleSerializer(self._role4).data
        self._role5_json = SubjectUserRoleSerializer(self._role5).data
        self._role6_json = SubjectUserRoleSerializer(self._role6).data

        self.urls = {
            "get_student_count": reverse('subjects-get-student-count') + '?id={}',
            "get_cross_subject_collision": reverse('subjects-get-cross-subject-collisions') + '?subject_a={}&subject_b={}',
            "make_cross_subject_collision": reverse('subjects-make-cross-subject-collisions')
        }

    def get_student_count(self, subject_id, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('get', self.urls['get_student_count'], None, exp_status_code, exp_message, subject_id)

    def get_cross_subject_collisions(self, subject_a, subject_b, exp_status_code, exp_message=None):
        url = self.urls['get_cross_subject_collision'].format(subject_a, subject_b)
        return self.exec_and_validate_req('get', url, None, exp_status_code, exp_message, None)

    def make_cross_subject_collisions(self, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('put', self.urls['make_cross_subject_collision'], None, exp_status_code, exp_message, None)

    # Test cases
    def test_get_student_count(self):
        res1 = self.get_student_count(self._subject1_json['id'], status.HTTP_200_OK)
        exp_res_data1 = {"subject_id": self._subject1_json['id'], "student_count": 2}
        self.assert_response_data(res1.data, [exp_res_data1])

        res2 = self.get_student_count(self._subject2_json['id'], status.HTTP_200_OK)
        exp_res_data2 = {"subject_id": self._subject2_json['id'], "student_count": 2}
        self.assert_response_data(res2.data, [exp_res_data2])

        res3 = self.get_student_count(self._subject3_json['id'], status.HTTP_200_OK)
        exp_res_data3 = {"subject_id": self._subject3_json['id'], "student_count": 1}
        self.assert_response_data(res3.data, [exp_res_data3])

        res4 = self.get_student_count(100, status.HTTP_200_OK)
        exp_res_data4 = {"subject_id": 100, "student_count": 0}
        self.assert_response_data(res4.data, [exp_res_data4])

        res5 = self.get_student_count('', status.HTTP_200_OK)
        self.assert_response_data(res5.data, [exp_res_data1, exp_res_data2, exp_res_data3])

        self.assert_model_state(Subject, 3, [self._subject1_json, self._subject2_json, self._subject3_json])

        self.assert_model_state(User, 2, [self._student1_json, self._student2_json])

        self.assert_model_state(SubjectUserRole, 6, [ self._role1_json, self._role2_json, self._role3_json,
                self._role4_json, self._role5_json, self._role6_json])

    def test_cross_subject_collisions(self):
        # Preparing models
        self.make_cross_subject_collisions(status.HTTP_200_OK, "Materialized view has been successfully created.")

        # Testing
        res = self.get_cross_subject_collisions(self._subject1_json['id'], self._subject2_json['id'], status.HTTP_200_OK)
        exp_res1 = {
            "subject_a": self._subject1_json['id'],
            "subject_b": self._subject2_json['id'],
            "collision_count": 2
        }
        self.assert_response_data(res.data, exp_res1)

        res = self.get_cross_subject_collisions(self._subject1_json['id'], self._subject3_json['id'], status.HTTP_200_OK)
        exp_res2 = {
            "subject_a": self._subject1_json['id'],
            "subject_b": self._subject3_json['id'],
            "collision_count": 1
        }
        self.assert_response_data(res.data, exp_res2)

        res = self.get_cross_subject_collisions(self._subject2_json['id'], self._subject3_json['id'], status.HTTP_200_OK)
        exp_res3 = {
            "subject_a": self._subject2_json['id'],
            "subject_b": self._subject3_json['id'],
            "collision_count": 1
        }
        self.assert_response_data(res.data, exp_res3)

        self.get_cross_subject_collisions('', self._subject2_json['id'], status.HTTP_400_BAD_REQUEST, "Both subject_a and subject_b parameters are required.")
        self.get_cross_subject_collisions(self._subject1_json['id'], '', status.HTTP_400_BAD_REQUEST, "Both subject_a and subject_b parameters are required.")
        self.get_cross_subject_collisions('', '', status.HTTP_400_BAD_REQUEST, "Both subject_a and subject_b parameters are required.")

        self.get_cross_subject_collisions(self._subject1_json['id'], 100, status.HTTP_404_NOT_FOUND, "No collision found for the given subject pair.")
        self.get_cross_subject_collisions(100, self._subject2_json['id'], status.HTTP_404_NOT_FOUND, "No collision found for the given subject pair.")
        self.get_cross_subject_collisions(100, 200, status.HTTP_404_NOT_FOUND, "No collision found for the given subject pair.")

        self.make_cross_subject_collisions(status.HTTP_200_OK, "Materialized view has been successfully refreshed.")

        self.assert_model_state(Subject, 3, [self._subject1_json, self._subject2_json, self._subject3_json])

        self.assert_model_state(User, 2, [self._student1_json, self._student2_json])

        self.assert_model_state(SubjectUserRole, 6, [ self._role1_json, self._role2_json, self._role3_json,
                self._role4_json, self._role5_json, self._role6_json])

        self.assert_model_state(SubjectUserCollision, 3, [exp_res1, exp_res2, exp_res3])
