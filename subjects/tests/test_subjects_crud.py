# ./subjects/tests/test_subjects_crud.py
from django.urls import reverse

from rest_framework import status

from subjects.models import Subject
from subjects.services.serializers import SubjectSerializer
import test.helpers


class SubjectCrudTestCase(test.helpers.TestBase):
    def setUp(self):
        self._subject1 = Subject.objects.create(code="T-Test1", name="Test Subject1", nominal_semester=1)
        self._subject1_json = SubjectSerializer(self._subject1).data

        self.urls = {
            "create": reverse('subjects-create-subject'),
            "get_subject": reverse('subjects-get-subject') + '?id={}',
            "get_subjects": reverse('subjects-get-subjects') + '?ids={}',
            "update": reverse('subjects-update-subject') + '?id={}',
            "delete": reverse('subjects-delete-subject') + '?id={}',
        }

    def create_subject(self, data, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('post', self.urls['create'], data, exp_status_code, exp_message)

    def get_subject(self, subject_id, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('get', self.urls['get_subject'], None, exp_status_code, exp_message, subject_id)

    def get_subjects(self, subject_ids, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('get', self.urls['get_subjects'], None, exp_status_code, exp_message, subject_ids)

    def update_subject(self, subject_id, data, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('put', self.urls['update'], data, exp_status_code, exp_message, subject_id)

    def delete_subject(self, subject_id, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('delete', self.urls['delete'], None, exp_status_code, exp_message, subject_id)

    def prepare_data(self, code=None, name=None, nominal_semester=None):
        subject_json = SubjectSerializer(
            Subject(code=code, name=name, nominal_semester=nominal_semester)
        ).data

        return {key: value for key, value in subject_json.items() if value is not None}

    # Test cases
    def test_create_subject(self):
        subject2_json = self.prepare_data("T-Test2", "Test Subject2", 2)

        res = self.create_subject(subject2_json, status.HTTP_201_CREATED)
        self.assert_response_data(res.data, subject2_json)

        invalid_data1 = self.prepare_data(name="Test Subject3")
        self.create_subject(invalid_data1, status.HTTP_400_BAD_REQUEST, "Invalid data provided.")

        invalid_data2 = self.prepare_data(code="Test Code")
        self.create_subject(invalid_data2, status.HTTP_400_BAD_REQUEST, "Invalid data provided.")

        invalid_data3 = self.prepare_data()
        self.create_subject(invalid_data3, status.HTTP_400_BAD_REQUEST, "Invalid data provided.")

        # Trying to create a subject which already exists (duplicate)
        self.create_subject(subject2_json, status.HTTP_400_BAD_REQUEST, "Invalid data provided.")

        self.assert_model_state(Subject, 2, [self._subject1_json, subject2_json])

    def test_get_subject(self):
        res = self.get_subject(self._subject1_json['id'], status.HTTP_200_OK)
        self.assert_response_data(res.data, self._subject1_json)

        self.get_subject(0, status.HTTP_404_NOT_FOUND, "Subject not found.")

        self.get_subject('', status.HTTP_400_BAD_REQUEST, "No id provided.")

        self.assert_model_state(Subject, 1, [self._subject1_json])

    def test_get_subjects(self):
        # Adding a new subject
        subject2_json = self.prepare_data("T-Test2", "Test Subject2", 2)
        res = self.create_subject(subject2_json, status.HTTP_201_CREATED)

        # To include 'id' as well
        subject2_json = res.data

        # Testing
        res1 = self.get_subjects([self._subject1_json['id'], subject2_json['id']], status.HTTP_200_OK)
        self.assert_response_data(res1.data, [self._subject1_json, subject2_json])

        res2 = self.get_subjects('', status.HTTP_200_OK)
        self.assert_response_data(res2.data, [self._subject1_json, subject2_json])

        res3 = self.get_subjects([self._subject1_json['id']], status.HTTP_200_OK)
        self.assert_response_data(res3.data, [self._subject1_json])

        res4 = self.get_subjects([subject2_json['id']], status.HTTP_200_OK)
        self.assert_response_data(res4.data, [subject2_json])

        self.get_subjects([100, 200], status.HTTP_404_NOT_FOUND, "Subjects not found.")

        self.get_subjects(['a', 'b'], status.HTTP_400_BAD_REQUEST, "Invalid ID format.")

        self.assert_model_state(Subject, 2, [self._subject1_json, subject2_json])

    def test_update_subject(self):
        update_data = self.prepare_data("T-Test2", "Test Subject2", 2)

        res = self.update_subject(self._subject1_json['id'], update_data, status.HTTP_200_OK)
        self.assert_response_data(res.data, update_data)
        self.assert_model_state(Subject, 1, [update_data])

        partial_update_data = self.prepare_data(name="Test Subject2 Updated")

        exp_data_after_partial_update = update_data.copy()
        exp_data_after_partial_update.update(partial_update_data)

        res = self.update_subject(self._subject1_json['id'], partial_update_data, status.HTTP_200_OK)
        self.assert_response_data(res.data, exp_data_after_partial_update)
        self.assert_model_state(Subject, 1, [exp_data_after_partial_update])

        self.update_subject('', None, status.HTTP_400_BAD_REQUEST, "No id provided.")

        self.update_subject(0, update_data, status.HTTP_404_NOT_FOUND, "Subject not found.")

    def test_delete_subject(self):
        self.delete_subject(self._subject1_json['id'], status.HTTP_204_NO_CONTENT)

        self.delete_subject(self._subject1_json['id'], status.HTTP_404_NOT_FOUND, "Subject not found.")

        self.delete_subject('', status.HTTP_400_BAD_REQUEST, "No id provided.")

        self.assert_model_state(Subject, 0, [])
