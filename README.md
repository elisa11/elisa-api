


# Elisa-backend: 
<img src="readme.src/elisaLogo.png" alt="Sample Image" width="200"/>

produced by ChatGPT4o


## Backend installation:
This should cover how to run dev server on **local host** or in **docker**.

### Prerequirements: 
- `python3` (3.10+)
- `pip install -r requirements.txt` 
  - for python packages compilation install c libs. e.g. libpq-dev (required by psycopg2)
- postgress 17+

or Docker composer.


## Launch bare metal
0. Optional: make python virtual environment and source correct environment script for your shell. 
```shell
python -m venv venv 
source venv/bin/activate # or activate.csh or activate.fish or activate.ps1
```
  - if unsure what to do next, follow `./entrypoint.sh` 
1. ***Run command:*** `python manage.py generate_env` to generate predefined `.env` file.  
*this command extracts all os.getenv(key, default) calls and make env file with KEY=DEFAULT pairs.*   

2. ***Rename file:*** `EXPORTED.env`  *==to=>* `.env`   
*(or any other path, but don't forget change call `load_dotenv(dotenv_path=yours)` in elisa/settings.py)*   
3. ***Set your variables*** in `.env` like: DB connection, LDAP connection, LOG levels and output files. etc.
  
    ``` bash
    # logging levels  # DEBUG,INFO,WARNING,ERROR,CRITICAL
    DEBUG=True
    LOG_LEVEL_DJANGO="INFO"
    LOG_FILE_PREFIX_DJANGO="django" # extention added automatically
    
    LOG_LEVEL_DB="DEBUG"
    LOG_FILE_PREFIX_DB="db"
    
    LOG_LEVEL_LDAP="INFO"
    LOG_FILE_PREFIX_LDAP="ldap" # extention added automatically
    
    # localstorage directory
    APPDATA='.DATA'
    # DB: DEV
    DB_NAME='dev'
    DB_USER='elisa'
    DB_PASSWORD='elisaelisa'
    DB_HOST='localhost' #or 'db' for docker-compose
    DB_PORT='5432'
    ...
    ```
4. ***Ensure DB connection*** in .env represents your postgres DB connection.
5. *** Setup PostgreSQL 17: *you are on your own*, or `docker compose up db`
   - ( this step leads to .env:`DB_HOST="localhost"`)
6. ***Create tables for public schema :*** `python manage.py migrate_apps --VARLIST STATIC_APPS`   
7. ***Run dev server:** `python manage.py runserver`
8. visit: http://localhost:8000/public/spec/swagger

## Launch (Docker):
0. Optional: entrypoint.sh: creates .env file, with default values, if desired different feel free to edit.
    editing env variables need to correspond with docker-compose.yaml/db such as DB host,db name, user,pass,...
1.  If both run in docker, ensure .env:`DB_HOST="db"`,)     
2. `docker-compose build`
3. `docker-compose up`
4. visit http://localhost:8000/public/spec/swagger

## Best for develop:
Pycharm: [Launch bare metal](#Launch bare metal), with db in docker.


## Clean docker images/volumes
```
# List all containers, including stopped ones, to find the container ID using the volume
sudo docker ps -a

# Stop the container using the volume (replace CONTAINER_ID with the actual container ID)
sudo docker stop CONTAINER_ID

# Remove the container using the volume
sudo docker rm CONTAINER_ID

# Remove the volume
sudo docker volume rm elisa-api_pgdata  #{see docker-compose.yml/db/volume}
```

# Documentation
Target: FE devs

After docker says:
```
Django version 4.2.16, using settings 'elisa.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```
you can proceed [testing](#Testing) endpoints from api.

## Schemas
Schema is a database schema. Postgres database `dev` contains multiple schemas like `public`   
`information_schema` `pg_catalog` etc. We discuss here `public` schema and ones created by user.

### Public
This schema contains objects that live outside academic [Term](#Term)s and are not expected to get removed   
or change relations.   
*As of now* there are tables: `user_user` (+permissions group), django's tables, 
and tables provided by django-tenant-schemas `tenantManager_domain` and `tenantManager_schema`.   
***user_user*** - expected to get hugely imported once and later imports should resolve merge conflicts.   
***tenantManager_schema*** - holds (name-created_at) information about user created schemas. 
There is slight modification apart from default django-tenant-schemas. We use only localhost domain, where tenants
are [Term](#Term)s 

## Term
A term groups subjects + relations. Since in each term there is new set of subject    
with user-subject-role relations, for good separation and DB lookup times it is stored as different *schema*.   
In [Public](#public) schema lives user (and possibly their preferences). Term specific like `subject, timetable,..`  
lives in separate schema.  
Which endpoints are term specific? - in swagger.yaml defines header X-term. If endpoint works with   
any object that is term specific caller must specify X-term.

So, operations like CRUD on user do not require X-term header, because they can be directly accessed.    
Operations like un/assigning user to subject with role ("delete role CVICIACI of JokaY from OS") need to   
specify X-term, because subject is Term specific.

Rule of thumb: if entity would be removed, would it break last-years timetable?   
(room: yes, subject: yes, user: *shouldn't be removed* no-especially if student)

# Testing
### Use case: import db from .DATA dir
## API Call 1: Create User
Description: This API call creates a new user.

### Request
*hint: in Linux terminal press CTRL+D to send End Of Line signal, to end prompt*
```bash
# Send POST body {"term":"test"} to /api/schemas to create new Term schema 
POST http://localhost:8000/api/schemas/ -d '{"term":"test"}'
```
```bash
# get all schemas, to verify creation
GET http://localhost:8000/api/schemas/
```

```bash
# navigate to your data via ls at /api/import/lsData
GET http://localhost:8000/api/import/lsData/?=/
GET http://localhost:8000/api/import/lsData/?=/ais.2024-4-18
#this should list csv files. so the path is "/ais.2024-4-18"
```
```bash
# import terms overview from directory
POST http://localhost:8000/api/imports_exports/fei/terms/import/dir/?path=%2Fais.2024-4-18
#list terms (with filters)
GET api/imports_exports/fei/terms/?department=FEI&limit=15&offset=15&ordering=-year_start
```
```bash
# lets import term into db
POST http://localhost:8000/api/imports_exports/fei/import/dir/?path=%2Fais.2024-4-18&term=629 \
  -H 'X-Term: test'
```
```bash
# now lets list counts where subjects share students (with filters)
GET http://localhost:8000/api/subject-user-collisions/?ordering=collision_count&subject_a=379814' \
  -H 'X-Term: test'
```

Now we can manipulate data inside, or remake clean schema:
remake:
from get terms we know:
`{
    "id": 629,
    "semester": "1",
    "year_start": "2023", 
    "phd": false,
    "department": "FEI",
    "prev": 628,
    "next": 630,
    "start_date": DateTime
    "end_date": DateTime
  },
`    

DB may look like this:    
<img src="readme.src/db_schemas.png" alt="DB schemas example" width="200"/>    

Let's  try getting count on subject (B-OS: 379814):
```bash
GET http://localhost:8000/api/subjects/get_student_count/?subject_id=379814 \
  -H "X-Term: term_LS_2023_2024"
# or not specified subject_id, get all counts.
```

