from django.urls import include, path
from .views import ImportViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'import', ImportViewSet, basename='import')


urlpatterns = [
    path('imports_exports/fei/', include('imports_exports.fei.urls')),
    #path('api/imports_exports/newApp/', include('newApp.urls')),
] + router.urls
