import os

from drf_spectacular.utils import OpenApiParameter, OpenApiResponse, extend_schema
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from elisa.middleware import logger

@extend_schema(tags=["Import"])
class ImportViewSet(viewsets.ViewSet):
    """
    A ViewSet for retrieving and listing users.
    """

    permission_classes = [] # [AuthClasses]

    @extend_schema(
        summary="List contents of a directory in the DATA directory",
        description="Lists all files and directories in the specified path under the DATA directory.",
        parameters=[
            OpenApiParameter(name='path', description='Specifies the target directory path', required=True, type=str),
        ],
        responses={
            200: OpenApiResponse(
                description="Directory contents",
                examples={
                    'application/json': {
                        "dirs": ['dir1', 'dir2','dir3'],
                        "files": ['file1.txt', 'file2.csv']
                    }
                }
            ),
            400: OpenApiResponse(description="The specified path points to a file."),
            404: OpenApiResponse(description="No such path or directory."),
        }
    )
    @action(detail=False, methods=['get'], url_path='lsData')
    def lsData(self, request):
        """
        Lists contents of the specified directory inside the DATA directory.

        Args:
            request: The HTTP request object.
        Query Parameters:
            path (string): The target path relative to the DATA directory.

        Returns:
            200: A JSON list of directory contents (files and folders separately).
            400: If the path points to a file instead of a directory.
            404: If the path does not exist.
        """
        path = request.query_params.get('path', '/')
        data_directory = os.getenv("APPDATA", ".DATA")
        target_path = os.path.join(data_directory, path.strip('/'))
        logger.log(20,f"listing contents of {target_path}")
        if not os.path.exists(target_path):
            return Response({"error": "No such path or directory."}, status=404)
        if os.path.isfile(target_path):
            return Response({"error": "The specified path points to a file."}, status=400)
        try:
            contents = os.listdir(target_path)
            dirs = [name for name in contents if os.path.isdir(os.path.join(target_path, name))]
            files = [name for name in contents if os.path.isfile(os.path.join(target_path, name))]
        except Exception as e:
            return Response({"error": str(e)}, status=400)

        # Return the directory and file contents
        return Response({"dirs": dirs, "files": files}, status=200)
    
    
