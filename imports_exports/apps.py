from django.apps import AppConfig


class ImportsExportsConfig(AppConfig):
    name = 'imports_exports'
