from django.apps import AppConfig


class FeiConfig(AppConfig):
    name = 'imports_exports.fei'