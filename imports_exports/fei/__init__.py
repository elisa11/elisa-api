import os
FEI_IMPORT_DB =  {
    'ENGINE': 'django.db.backends.oracle',
    'NAME': os.getenv('DB_IMPORT_NAME'),
    'USER': os.getenv('DB_IMPORT_USER'),
    'PASSWORD': os.getenv('DB_IMPORT_PASSWORD'),
    # 'HOST': 'db-new.is.stuba.sk',
    'HOST': os.getenv('DB_IMPORT_HOST'),
    'PORT': os.getenv('DB_IMPORT_PORT', '1521'),
}