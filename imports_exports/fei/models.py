from django.db import models

from django_filters import rest_framework as filters
from django_tenants.utils import schema_context
from rest_framework import serializers

SEMESTER_CHOICE = (
    ('ZS', 'Zimny'),
    ('LS', 'Letny'),
)

class PublicAISOdobie(models.Model):
    """
    AIS term model that lives in the public schema and is accessible to all tenants
    """

    id = models.IntegerField(primary_key=True)
    semester = models.CharField(max_length=2, choices=SEMESTER_CHOICE)
    year_start = models.CharField(max_length=9)
    phd = models.BooleanField(default=False)
    department = models.CharField(max_length=10)
    prev = models.IntegerField(null=True)
    next = models.IntegerField(null=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()

    semester = models.CharField(max_length=2, choices=SEMESTER_CHOICE)
    class Meta:
        managed = True
        # use public schema
        db_table = 'ie_aisodobie'
        verbose_name = 'AIS obdobie'
        verbose_name_plural = 'AIS obdobia'
        ordering = ['id']
        indexes = [
            models.Index(fields=['semester', 'year_start']),
        ]

    # @classmethod
    # def create_from_import(cls, data):
    #     """Helper method to create from import data"""
    #     with schema_context('public'):
    #         return cls.objects.create(**data)
    #
    # @classmethod
    # def get_all_terms(cls):
    #     """Get all terms from public schema"""
    #     with schema_context('public'):
    #         return cls.objects.all()

