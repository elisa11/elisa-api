import datetime
import os
import shutil
import zipfile
from typing import Dict, Literal
import csv
from io import StringIO

import pandas as pd
from django.conf import settings
from django.db import connection
from django.http.request import RAISE_ERROR
from django_tenants.utils import schema_context

from building.models import Building
from elisa.middleware import logger
from elisa.settings import BATCH_SIZE
from imports_exports.fei.models import PublicAISOdobie, SEMESTER_CHOICE
from rooms.models import Equipment, Room, RoomEquipment
from subjects.models import Subject, SubjectGroup, SubjectUserRole
from timetable.models import (Allowance, TTEventType)
from users.models import User
from django.db import IntegrityError, transaction


def import_room_equipment(dataframes, import_term=None):
    rooms_df = dataframes['fei_rooms.csv']
    dep_df = dataframes['fei_departments.csv']
    equip_df = dataframes['fei_equipments.csv']
    room_equip_df = dataframes['fei_rooms_equipments.csv']

    # transforming fei_rooms.csv into elisa.room model
    #rooms_df_db = pd.merge(rooms_df, dep_df, left_on='PRACOVISKO', right_on='ID', how='left')
    #rooms_df_db = rooms_df_db[['ID_x','ID_y', 'NAZOV_x', 'KAPACITA', 'SKRATKA']] # filter columns
    #rooms_df_db.columns = ['id', 'name', 'capacity', 'building'] # renaming columns to bulk insert to db
    # bulk insert rooms into elisa.models.Room



    buildings = [
        Building(id=row.ID,
                 name=row.NAZOV,
                 abbrev=row.SKRATKA)
        for row in dep_df.itertuples(index=False)
    ]
    Building.objects.bulk_create(buildings, ignore_conflicts=True)

    rooms = [
        Room(id=row.ID,
             name=row.NAZOV,
             capacity=row.KAPACITA,
             building_id=row.PRACOVISKO)
        for row in rooms_df.itertuples(index=False)
    ] # list comp. faster then for
    Room.objects.bulk_create(rooms, ignore_conflicts=True)

    equipments = [
        Equipment(id=row.ID, name=row.NAZOV)
        for row in equip_df.itertuples(index=False)
    ]
    Equipment.objects.bulk_create(equipments,ignore_conflicts=True)


    rooms_equipments = [
        RoomEquipment(room_id=row.MIESTNOST, equipment_id=row.POMOCKA,count=row.POCET)
        for row in room_equip_df.itertuples(index=False)
    ]
    RoomEquipment.objects.bulk_create(rooms_equipments,ignore_conflicts=True)


def import_subjects(dfs, import_term):
    # s = subject, p = program,

    # term  = AIS_obdobie.objects.get(pk=int(import_term))

    study_plans_df  = dfs['fei_study_plans.csv']
    # _  = dfs['fei_study_programs.csv']
    subjects_df  = dfs['fei_subjects.csv']
    subjects_df = subjects_df[subjects_df['OBDOBIE_ID']==int(import_term)]
    # _  = dfs['fei_groups.csv']
    # _  = dfs['fei_study_programs_supervisors.csv']
    # _  = dfs['fei_study_programs_tracks.csv']
    # _  = dfs['fei_users_groups.csv']
    ais_ttevent_type_df = dfs['c_typ_rozvrh_akcie.csv']
    sallowence_df = dfs['fei_subjects_allowance.csv']
    # Filter allowence_df to keep only rows where 'subject' exists in the 'id' column of subjects_df
    sallowence_df = sallowence_df[sallowence_df['PREDMET'].isin(subjects_df['ID'])]

    subjects_with_sem_df = pd.merge(subjects_df, study_plans_df[['KOD','SEMESTER_PLANU','PROGRAM']],
                                    left_on='KOD', right_on='KOD', how='left')
    subjects_with_sem_df = subjects_with_sem_df.groupby('KOD', as_index=False).first()
    subjects_with_sem_df.fillna(0, inplace=True)

    subjects = [Subject(id=row.ID,
                        code=row.KOD,
                        name=row.NAZOV,
                        nominal_semester=row.SEMESTER_PLANU,
                        building_id=row.GARANTUJUCE_PRACOVISKO)
        for row in subjects_with_sem_df.itertuples(index=False)
    ]
    try:
        with transaction.atomic():
            Subject.objects.bulk_create(subjects,batch_size=BATCH_SIZE, ignore_conflicts=False)
    except Exception as e:
        print(f"Error occurred: {e}")

    subject_groups = [SubjectGroup(subject_id=row.ID, name=row.PROGRAM,)
                    for row in subjects_with_sem_df.itertuples(index=False)
                    ]
    try:
        with transaction.atomic():
            SubjectGroup.objects.bulk_create(subject_groups, batch_size=BATCH_SIZE, ignore_conflicts=False)
    except Exception as e:
        print(f"Error occurred: {e}")

    ttypes = [
        TTEventType(id=row.ID,
                    name=row.NAZOV)
        for row in ais_ttevent_type_df.itertuples(index=False)
    ]
    try:
        with transaction.atomic():
            TTEventType.objects.bulk_create(ttypes, batch_size=BATCH_SIZE, ignore_conflicts=True)
    except Exception as e:
        print(f"Error occurred: {e}")

    allowance = [Allowance(event_type_id=row.TYP_AKCIE,
                           subject_id=row.PREDMET,
                           amount=row.HODINY)
        for row in sallowence_df.itertuples(index=False)
    ]
    try:
        with transaction.atomic():
            Allowance.objects.bulk_create(allowance, batch_size=BATCH_SIZE, ignore_conflicts=True)
    except Exception as e:
        print(f"Error occurred: {e}")


def import_users(dfs, import_term=None):
    users_df = dfs['fei_users.csv']
    users_groups_df = dfs['fei_users_groups.csv']
    sub_user_df  = dfs['fei_subjects_users.csv']
    subs_df  = dfs['fei_subjects.csv']
    subs_df = subs_df[subs_df['OBDOBIE_ID']==int(import_term)]

    users = [
        User(id=row.AIS_ID,
             username=row.LOGIN,
             full_name=row.MENO + ' ' + row.PRIEZVISKO,
             last_login=None,
             last_logout=None,
             )
        for row in users_df.itertuples(index=False)
    ]
    current_schema = None
    with connection.cursor() as cursor:
        cursor.execute("SHOW search_path")
        current_schema = cursor.fetchone()[0]
        cursor.execute("SET search_path TO public")
        User.objects.bulk_create(users, ignore_conflicts=True)
        cursor.execute(f"SET search_path TO {current_schema}")

    sub_user_df = sub_user_df[sub_user_df['PREDMET'].isin(subs_df['ID'])]
    sub_user_df.loc[:, 'ROLA'] = sub_user_df['ROLA'].str.split(', ')
    sub_user_df = sub_user_df.explode('ROLA')
    sub_user_df.drop_duplicates(inplace=True)
    subs_users = [
        SubjectUserRole(name=row.ROLA,
                        user_id=row.AIS_ID,
                        subject_id=row.PREDMET)
        for row in sub_user_df.itertuples(index=False)
    ]
    SubjectUserRole.objects.bulk_create(subs_users, ignore_conflicts = True)




def import_terms(dfs):
    terms_df = dfs['ais_obdobia.csv'][["ID", "NAZOV_SK", "PRACOVISKO",
                                       "POR_AR","PREDCHADZAJUCE","NASLEDUJUCE",
                                       "ZACIATOK","KONIEC","AKTIVNE"]]
    dep_df = dfs['fei_departments.csv']

    terms_merged = pd.merge(terms_df, dep_df, left_on='PRACOVISKO', right_on='ID', how='left')
    terms_merged = terms_merged[['ID_x', "NAZOV_SK", "SKRATKA",
                                       "POR_AR","PREDCHADZAJUCE","NASLEDUJUCE",
                                       "ZACIATOK","KONIEC","AKTIVNE"]]  # filter columns
    terms_merged.fillna(value=0,inplace=True) # todo find normal null value
    terms_merged['PHD'] = terms_merged['NAZOV_SK'].str.contains(' - doktor', case=False)
    terms_merged['NAZOV_SK'] = terms_merged['NAZOV_SK'].str.extract(r'(\d{4}/\d{4})')
    terms_to_import = [
        PublicAISOdobie(
            id=row.ID_x,
            semester=row.POR_AR,
            yearStr=row.NAZOV_SK,
            phd=row.PHD,
            department=row.SKRATKA,
            prev=row.PREDCHADZAJUCE,
            next=row.NASLEDUJUCE,
            start_date=datetime.datetime.strptime(row.ZACIATOK, "%d.%m.%y"),  # Transform start date
            end_date=datetime.datetime.strptime(row.KONIEC,"%d.%m.%y"),  # Transform end date
        )
        for row in terms_merged.itertuples(index=False)
    ]
    PublicAISOdobie.objects.bulk_create(terms_to_import, ignore_conflicts=True)



class FeiImporter:
    """
    Handles the core logic for importing data into the system.
    """

    def __init__(self, conn_type: Literal['zip', 'dir', 'db'], content=None):
        self.type = conn_type
        self.content = content


    def import_elisa(self, term=None, persist=False):
        """
        if term not provided, import just term. Next time call with term param.
        """
        match self.type:
            case 'zip':
                self.import_from_zip(persist,term)
            case 'dir':
                self._import_csvs(self.content,term)
            case 'db':
                RAISE_ERROR('FeiImporter type not supported YET')

            case _:
                RAISE_ERROR('FeiImporter type not supported')
        if term:
            return "Successfully imported: Elisa"
        else:
            return "Successfully imported: AIS_terms"


    def import_from_zip(self, term, persist=False ):
        """
        Import data directly from an uploaded ZIP file.
        """
        # dump zip into APPDATA
        if persist:
            save_path = os.path.join(settings.BASE_DIR, os.getenv('APPDATA'), f'{self.content.name}_{datetime.datetime.now().isoformat()}')
            with open(save_path, 'wb+') as destination:
                for chunk in self.content.chunks():
                    destination.write(chunk)
        # extract zip
        with zipfile.ZipFile(self.content, 'r') as zip_ref:
            temp_dir = os.path.join(settings.BASE_DIR, 'tmp_import')
            zip_ref.extractall(temp_dir)
            self._import_csvs(temp_dir, term) # import via CSV tmp dir
            shutil.rmtree(temp_dir)


    def import_from_db(self):
        """
        Connect to the live FEI database and import data.
        """
        # Example: Use .env for DB connection details
        # need to write logic to connect to the FEI database, fetch data, and import it
        pass

    def _detect_separator(self, file_path: str) -> str:
        """
        Detect the delimiter/separator used in a CSV file.
        Returns the detected separator or falls back to ',' if detection fails.
        """
        try:
            with open(file_path, 'r', newline='') as csvfile:
                # Read a sample of the file (first few lines)
                sample = csvfile.read(4096)
                
                # Try to detect the dialect
                dialect = csv.Sniffer().sniff(sample)
                return dialect.delimiter
        except Exception as e:
            # Log the error and fall back to comma
            logger.warning(f"Failed to detect separator for {file_path}: {str(e)}. Falling back to ','")
            return ','

    def _load_from_dir(self, dir_path: str) -> Dict[str, pd.DataFrame]:
        """
        Load all CSV files from the specified directory into a dictionary of DataFrames.
        """
        dataframes = {}
        for file_name in os.listdir(dir_path):
            if file_name.endswith('.csv'):
                file_path = os.path.join(dir_path, file_name)
                separator = self._detect_separator(file_path)
                dataframes[file_name] = pd.read_csv(file_path, sep=separator)
        return dataframes

    def _import_csvs(self, dir_path, term):
        """
        Load all CSV files from the specified directory into a dictionary of DataFrames.

        Args:
            dir_path (str): The path to the directory containing CSV files.

        Returns:
            dict: A dictionary where the keys are file names (without .csv) and the values are DataFrames.
        """
        dataframes = self._load_from_dir(dir_path)

        if not term:
            import_terms(dataframes)
        else:
            import_room_equipment(dataframes, import_term=term)
            import_subjects(dataframes, import_term=term)
            import_users(dataframes, import_term=term)   # people depend on subjects, that's why they are here



        return "ok"

    def import_terms(self) -> str:
        """
        Import only terms data from the source.
        Returns a success message or raises an exception.
        """
        # load dataframes
        # check if all files are present
        term_path = os.path.join(self.content, 'ais_obdobia.csv')
        dep_path = os.path.join(self.content, 'fei_departments.csv')
        if not os.path.exists(term_path):
            raise FileNotFoundError("ais_obdobia.csv not found")
        if not os.path.exists(dep_path):
            raise FileNotFoundError("fei_departments.csv not found")

        terms_df = pd.read_csv(term_path, sep=self._detect_separator(term_path))
        dep_df = pd.read_csv(dep_path, sep=self._detect_separator(dep_path))

        terms_df = terms_df[["ID", "NAZOV_SK", "PRACOVISKO", "POR_AR", "KONIEC",
                             "PREDCHADZAJUCE", "NASLEDUJUCE", "ZACIATOK", "AKTIVNE"]]

        terms_merged = pd.merge(terms_df, dep_df, left_on='PRACOVISKO', right_on='ID', how='left')
        terms_merged = terms_merged[['ID_x', "NAZOV_SK", "SKRATKA",
                                     "POR_AR", "PREDCHADZAJUCE", "NASLEDUJUCE",
                                     "ZACIATOK", "KONIEC", "AKTIVNE"]]
        
        # Clean and transform data
        terms_merged.fillna(value=0, inplace=True)
        terms_merged['phd'] = terms_merged['NAZOV_SK'].str.contains(' - doktor', case=False)
        terms_merged['year_start'] = terms_merged['NAZOV_SK'].str.extract(r'(\d{4})/')

        # Convert dates
        terms_merged['start_date'] = pd.to_datetime(terms_merged['ZACIATOK'], format='%d.%m.%y')
        terms_merged['end_date'] = pd.to_datetime(terms_merged['KONIEC'], format='%d.%m.%y')
        # Prepare data for database
        terms_to_import = [
            PublicAISOdobie(
                id=row.ID_x,
                semester=SEMESTER_CHOICE[row.POR_AR-1][0],
                year_start=row.year_start,
                phd=row.phd,
                department=row.SKRATKA,
                prev=row.PREDCHADZAJUCE if row.PREDCHADZAJUCE != 0 else None,
                next=row.NASLEDUJUCE if row.NASLEDUJUCE != 0 else None,
                start_date=row.start_date,
                end_date=row.end_date,
            )
            for row in terms_merged.itertuples(index=False)
        ]
        # Save to database
        with schema_context('public'):
            with transaction.atomic():
                PublicAISOdobie.objects.all().delete()  # Clear existing terms
                PublicAISOdobie.objects.bulk_create(terms_to_import, ignore_conflicts=True)
        return f"Successfully imported {len(terms_to_import)} terms"




