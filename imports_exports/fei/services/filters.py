from django_filters import rest_framework as filters
from ..models import PublicAISOdobie

class AISObdobieFilter(filters.FilterSet):
    """
    Filter set for AIS terms in public schema
    """
    # Year filter with exact and contains options
    year_start = filters.CharFilter(
        field_name='year_start',
        lookup_expr='exact',
        help_text='Filter by exact year (e.g., 2023/2024)'
    )
    year_start_contains = filters.CharFilter(
        field_name='year_start',
        lookup_expr='contains',
        help_text='Filter by partial year (e.g., 2023)'
    )

    class Meta:
        model = PublicAISOdobie
        fields = {
            'id': ['exact', 'in',],
            'year_start': ['exact', 'in', 'gte', 'lte'],
            'semester': ['exact', 'in',],
            'department': ['exact', 'in', ],
            'phd': ['exact', ],
            'next': ['exact', ],
            'prev': ['exact', ],
        }
