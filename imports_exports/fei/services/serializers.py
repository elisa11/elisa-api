
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import serializers
from imports_exports.fei.models import PublicAISOdobie


class AISObdobieSerializer(serializers.ModelSerializer):
    """
    Serializer for AIS terms
    """
    class Meta:
        model = PublicAISOdobie
        fields = ['id', 'semester', 'year_start', 'phd', 'department',
                 'prev', 'next', 'start_date', 'end_date']
    
    EXAMPLE_RESPONSE = {
        "id": 692,
        "semester": "WS",
        "year_start": "2023",
        "phd" : True,
        "department": "FEI",
        "prev": 630,
        "next": 693,
        "start_date": "2023-09-01",
        "end_date": "2024-01-31"
    }