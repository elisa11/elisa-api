from django.urls import path, include
from .views import FeiImportViewSet, FeiExportViewSet, FeiTermViewSet
from rest_framework.routers import DefaultRouter

fei_router = DefaultRouter()
fei_router.register(r'import', FeiImportViewSet, basename='import')
fei_router.register(r'terms', FeiTermViewSet, basename='list-terms')
# fei_router.register(r'fei-export', FeiExportViewSet, basename='fei-export')

urlpatterns = [
    path('', include(fei_router.urls)),
]
