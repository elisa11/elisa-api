import logging
import os
import datetime
from typing import Literal

from django.conf import settings
from django.core.exceptions import BadRequest
from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import OpenApiParameter, extend_schema
from rest_framework import permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.filters import OrderingFilter
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from elisa.utils.global_oapi import term_schema_view
from elisa.utils.serializers import Err_serializer, Ok_serializer, err_response, ok_response
from .models import PublicAISOdobie
from .services.FeiImporter import FeiImporter
from .services.serializers import AISObdobieSerializer
from .services.filters import AISObdobieFilter
from tenantManager.decorators import public_view

logger = logging.getLogger(__name__)
@extend_schema(tags=["ImportExport/FEI: Imports"])
@term_schema_view()
class FeiImportViewSet(viewsets.ViewSet):
    """
    A ViewSet for importing fei data via directory, ZIP file, or live database.
    """
    # serializer_class = Ok_serializer
    @extend_schema(
        summary="Import FEI dataset from DATA directory",
        description="Import FEI exports from dir specified.",
        parameters=[
            OpenApiParameter(name='path', description='Specifies the target directory path', required=True, type=str),
            OpenApiParameter(name='term', description='Specifies the AIS term to import; if not specified, imports terms first, call GET get_terms', required=False, type=int),
        ],
        responses={
            200: Ok_serializer,
            400: Err_serializer,
            404: Err_serializer,
            500: Err_serializer,
        }

    )
    @action(detail=False, methods=['post'], url_path='dir')
    def import_from_dir(self, request):
        """
        Import data from a directory in the DATA folder.
        """
        path = request.query_params.get('path', None)
        term = request.query_params.get('term', None)
        if not path:
            return err_response("Path is required.", 400)
        full_path = os.path.join(settings.BASE_DIR, os.getenv('APPDATA'), path.strip('/'))

        if not os.path.isdir(full_path):
            return err_response("Path is not dir.", 400)

        if not os.path.exists(full_path):
            return err_response("Directory does not exist.", 404)

        # Call the import logic
        try:
            importer = FeiImporter(conn_type='dir', content=full_path)
            ret = importer.import_elisa(term=term)
        except Exception as e:
            return err_response(str(e), 500)

        return ok_response(ret)

    @extend_schema(
        summary="Import DB data from ZIP of CSVs",
        request={
            'multipart/form-data': {
                'type': 'object',
                'properties': {
                    'file': {
                        'type': 'string',
                        'format': 'binary',
                        'description': 'ZIP file containing CSV data',
                    },
                    'persist': {
                        'type': 'boolean',
                        'description': 'Whether to persist the ZIP in the DATA directory',
                        'default': False,
                    },
                    'term': {
                        'type': 'integer',
                        'description': 'AIS term to import',
                        'example': 2023,
                    },
                },
                'required': ['file'],  # Indicate that file is a required field
            },
        },
        responses={
            200: Ok_serializer,
            400: Err_serializer,
            500: Err_serializer,
        },
    )
    @action(detail=False, methods=['post'], url_path='zip', parser_classes=[MultiPartParser])
    def import_from_zip(self, request):
        """
        Import data from a ZIP file. Optionally persist it in the DATA directory.
        """
        zip_file = request.FILES.get('file', None)
        save_to_dir = request.data.get('persist', False)
        term = request.data.get('term', None)

        if not zip_file:
            return err_response("No file provided.", 400)

        # Optionally persist the ZIP in the DATA directory
        try:
            importer = FeiImporter(conn_type='zip', content=zip_file)
            importer.import_elisa(term=term, persist=save_to_dir)
        except FileNotFoundError as e:
            return err_response(str(e), 400)
        except Exception as e:
            return err_response(str(e), 500)

        return ok_response("Import from database successful")

    @extend_schema(
        summary="Import data from live AIS DB",
        parameters=[OpenApiParameter(name='id', description='Timetable ID', required=True, type=int)],
        responses={
            200: Ok_serializer,
            500: Err_serializer,
        },
    )
    @action(detail=False, methods=['post'], url_path='db')
    def import_from_db(self, request):
        """
        Import data by connecting to the live FEI database.
        """
        return err_response("not implemented", 501)
        # not implemented
        term = request.data.get('term', None)
        try:
            importer = FeiImporter(conn_type='db')
            importer.import_elisa(term)
        except Exception as e:
            return err_response(str(e), 500)

        return ok_response("not implemented")

class FeiExportViewSet(viewsets.ViewSet):
    serializer_class = Ok_serializer

    @action(detail=False, methods=['get'])
    def export_csv_view(self, request):
        # Transfer models -> elisa -> fei
        return ok_response("not implemented")

@extend_schema(tags=["ImportExport/FEI: Terms"])
class FeiTermViewSet(ReadOnlyModelViewSet):
    """
    ViewSet for retrieving and importing AIS terms.
    GET operations for terms data, POST operations for imports.
    """
    queryset = PublicAISOdobie.objects.all()
    serializer_class = AISObdobieSerializer
    filterset_class = AISObdobieFilter
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    ordering_fields = ['id', 'start_date', 'end_date', 'year_start', 'semester']
    ordering = ['-year_start']
    pagination_class = LimitOffsetPagination
    
    @extend_schema(
        summary="Import terms from directory",
        parameters=[
            OpenApiParameter(name='path', description='Directory path in DATA folder', required=True, type=str),
        ],
        request=None,
        responses={
            200: Ok_serializer,
            400: Err_serializer,
            404: Err_serializer,
            500: Err_serializer,
        }
    )
    @action(detail=False, methods=['post'], url_path='import/dir')
    def import_from_dir(self, request):
        """
        Import data from a directory in the DATA folder.
        """
        path = request.query_params.get('path', None)
        if not path:
            return err_response("Path is required.", 400)
        full_path = os.path.join(settings.BASE_DIR, os.getenv('APPDATA'), path.strip('/'))

        if not os.path.isdir(full_path):
            return err_response("Path is not dir.", 400)

        if not os.path.exists(full_path):
            return err_response("Directory does not exist.", 400)

        # Call the import logic
        try:
            importer = FeiImporter(conn_type='dir', content=full_path)
            ret = importer.import_terms()
        except FileNotFoundError as e:
            return err_response(str(e), 400)
        except Exception as e:
            return err_response(str(e), 500)

        return ok_response(ret)
    
    @extend_schema(
        summary="Import terms from ZIP file",
        request={
            'multipart/form-data': {
                'type': 'object',
                'properties': {
                    'file': {'type': 'string', 'format': 'binary'},
                    'persist': {'type': 'boolean', 'default': False},
                },
                'required': ['file']
            }
        },
        responses={
            200: Ok_serializer,
            400: Err_serializer,
            500: Err_serializer,
        }
    )
    @action(detail=False, methods=['post'], url_path='import/zip', parser_classes=[MultiPartParser])
    def import_from_zip(self, request):
        """
        Import data from a ZIP file. Optionally persist it in the DATA directory.
        """
        zip_file = request.FILES.get('file', None)
        save_to_dir = request.data.get('persist', False)
        term = request.data.get('term', None)

        if not zip_file:
            return err_response("No file provided.", 400)

        # Optionally persist the ZIP in the DATA directory
        try:
            importer = FeiImporter(conn_type='zip', content=zip_file)
            importer.import_elisa(term=term, persist=save_to_dir)
        except FileNotFoundError as e:
            return err_response(str(e), 400)
        except Exception as e:
            return err_response(str(e), 500)

        return ok_response("Import from database successful")
