#!/bin/bash
# Exit immediately if a command exits with a non-zero status
set -e

export_swagger() {
  echo "Exporting swagger..."
  python manage.py spectacular --file swagger.yaml
}
# Function to generate .env if it doesn't exist
generate_env() {
  if [ ! -f .env ]; then
    echo "Generating .env file..."
    python manage.py generate_env
    mv EXPORTED.env .env
  else
    echo ".env file already exists. Skipping generation. Trusting your config."
    echo "If you are unsure run $> python manage.py generate_env'"
    echo "this will generate GENERATED.env with all variables"
  fi
}

# Function to wait for the database to be ready
wait_for_db() {
  echo "Waiting for the database to be ready..."
  python manage.py wait_for_db
}
cleanmigrations(){
  echo "Cleaning migrations..."
  python manage.py cleanmigrations
}
# Function to apply migrations
apply_migrations() {
  echo "Migrating static apps..."
#  python manage.py migrate_apps --VARLIST STATIC_APPS
  python manage.py makemigrations_all
  python manage.py migrate
}

# Execute the functions
export_swagger
generate_env
wait_for_db
cleanmigrations
apply_migrations

# Execute the CMD from Dockerfile
exec "$@"
