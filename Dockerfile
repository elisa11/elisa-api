# nixer3/elisa-timetables = python:3.11-slim + deps in this file
# this should be as heavy base, so that later builds can
# use most of requirements.txt and just few new packages,
# which is faster then compiling 30 packages everytime.
FROM nixer3/elisa-timetables:001


ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /app

RUN apt-get update && apt-get install -y \
    gcc \
    libpq-dev \
    postgresql-client \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN chmod +x entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
