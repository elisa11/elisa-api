# ./test/settings.py
from django.conf.global_settings import TEST_RUNNER

from elisa.settings import *  # Import all base settings
from django.test.runner import DiscoverRunner
from django.db import connection

enableMigrations(True)

class CustomTestRunner(DiscoverRunner):
    def setup_databases(self, **kwargs):
        # Set up the test database but skip DYNAMIC_APPS
        test_databases = super().setup_databases(**kwargs)

        # Remove dynamic apps from synchronization
        with connection.cursor() as cursor:
            cursor.execute("SET search_path TO public")
            for app in DYNAMIC_APPS:
                print(f"Skipping migrations for dynamic app: {app}")
                # Ensure no tables are created for dynamic apps
                cursor.execute(f"DROP SCHEMA IF EXISTS test_{app} CASCADE;")
                cursor.execute(f"CREATE SCHEMA test_{app};")
        return test_databases

class DebugTestRunner(DiscoverRunner):

    def setup_databases(self, **kwargs):
        print("setup_databases args:")

        test_databases = super().setup_databases(**kwargs)
        # Debug: Show all schemas and tables
        with connection.cursor() as cursor:
            cursor.execute("SELECT schema_name FROM information_schema.schemata;")
            schemas = cursor.fetchall()
            print("Schemas in the test database:")
            for schema in schemas:
                print(schema[0])

            cursor.execute("""
                SELECT table_schema, table_name
                FROM information_schema.tables
                WHERE table_type = 'BASE TABLE'
                ORDER BY table_schema, table_name;
            """)
            tables = cursor.fetchall()
            print("Tables in the test database:")
            for table in tables:
                print(f"{table[0]}.{table[1]}")

        return test_databases

TEST_RUNNER = 'test.settings.DebugTestRunner'
