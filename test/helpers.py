# ./test/helpers.py
from django.test import TestCase

from urllib.parse import quote

from elisa.settings import DYNAMIC_APPS


class TestBase(TestCase):
    def make_request(self, method, url, data=None, query_ids=None):
            """
            Sends an HTTP request with the specified method, URL, and data.

            :param method: HTTP method (e.g., 'get', 'post').
            :param url: The request URL. Will be formatted with `query_id` if provided.
            :param data: (Optional) The data to send in the request body (JSON-serializable).
            :param query_ids: (Optional) An ID/IDs to format into the URL.

            :return: The HTTP response object.
            """

            if query_ids is not None:
                if isinstance(query_ids, list):
                    formatted_ids = ','.join(map(str, query_ids))
                    url = url.format(quote(formatted_ids))
                else:
                    url = url.format(query_ids)
            return getattr(self.client, method)(url, data, content_type='application/json')

    def exec_and_validate_req(self, method, url, data, exp_status_code, exp_message=None, query_ids=None):
        """
        Executes an HTTP request and validates the response status and message.

        :param method: The HTTP method.
        :param url: The URL for the request.
        :param data: The data to be sent in the request body.
        :param exp_status_code: The expected HTTP status code in the response.
        :param exp_message: (Optional) The expected message in the response.
        :param query_ids: (Optional) A query ID/IDs for the request.
        :return: The response object.
        """

        res = self.make_request(method, url, data=data, query_ids=query_ids)
        self.assert_response(res, exp_status_code, exp_message)
        return res

    def assert_response(self, res, exp_status_code, exp_message=None):
        """
        Asserts the response status and message.

        :param res: The response object to check.
        :param exp_status_code: The expected HTTP status code.
        :param exp_message: (Optional) The expected message in the response.

        :return: None.
        """

        self.assertEqual(res.status_code, exp_status_code)
        if exp_message:
            self.assertEqual(res.data.get('message'), exp_message)

    def _prepare_data_for_cmp(self, res, exp):
        if 'id' in res:
            del res['id']
        if 'id' in exp:
            del exp['id']

        return res, exp

    def assert_response_data(self, res_data, exp_data):
        """
        Compares response data with expected data.

        :param res_data: The actual response data.
        :param exp_data: The expected data to compare against.

        :return: None.
        """

        res_data, exp_data = self._prepare_data_for_cmp(res_data, exp_data)

        if isinstance(res_data, list):
            for i in range(len(res_data)):
                self.assertTrue(res_data[i] == exp_data[i])
        else:
            self.assertTrue(res_data == exp_data)

    def assert_model_state(self, model, exp_count, exp_entries):
        """
        Verifies if the number of database entries for a given model matches the expected count,
        and checks that these entries match the expected entries.

        :param model: The Django model class to check.
        :param exp_count: The expected number of entries in the model's database table.
        :param exp_entries: A list of dictionaries, where each dictionary represents the expected values
                                for a model instance's fields (e.g., {'username': 'alice', 'full_name': 'Alice Smith'}).

        :return: None.
        """

        self.assertEqual(model.objects.all().count(), exp_count)

        for entry in exp_entries:
            obj = model.objects.filter(**entry).first()

            self.assertIsNotNone(obj)


