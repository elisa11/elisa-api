# ./timetable/apps.py
from django.apps import AppConfig


class TimetableConfig(AppConfig):
    name = 'timetable'
