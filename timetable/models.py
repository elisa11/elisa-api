# ./timetable/models.py
from django.db import models, connection

from rooms.models import Room
from subjects.models import Subject
from users.models import User


class TT(models.Model):
    name = models.CharField(max_length=100, unique=True)  # ver 1y_api_preview_A
    program = models.CharField(max_length=25) # B-API
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    STATUS_CHOICES = [
        ('PUBLISHED', 'Published'),
        ('HIDDEN', 'Hidden'),
        ('WIP', 'Work In Progress'),
    ]
    status = models.CharField(
        max_length=10,
        choices=STATUS_CHOICES,
        default='WIP',
    )
    def save(self, *args, **kwargs):
        try:
            _ = self.owner
        except User.DoesNotExist:
            #TODO: for now just select first user
            self.owner = User.objects.first()
            # self.owner = kwargs.get('user')
        super().save(*args, **kwargs)


    def __str__(self):
        return f"{self.program} - {self.name}"


class TTEventType(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class TTEvent(models.Model):
    tt = models.ForeignKey(TT, on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    event_type = models.ForeignKey(TTEventType, on_delete=models.CASCADE)
    day_of_week = models.IntegerField(null=True)  # 1=Monday, 7=Sunday
    start_time = models.TimeField(null=True)
    duration = models.IntegerField(null=True)
    room = models.ForeignKey(Room, on_delete=models.CASCADE, null=True)
    weeks_bitmask = models.BigIntegerField(null=True)  # 64-bit integer representing weeks 1-64

    @property
    def everyweek_bitmask_binary(self):
        # Represent the bitmask as a binary string
        return bin(self.weeks_bitmask)[2:]
    
    def __str__(self):
        return f"{self.subject} - {self.event_type} on day {self.day_of_week} at {self.start_time}"

class UserTtEvent(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    tt_event = models.ForeignKey(TTEvent, on_delete=models.CASCADE)
    connection = models.CharField(max_length=25)

    class Meta:
        unique_together = [('user', 'tt_event', 'connection'),]

class Allowance(models.Model):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    event_type = models.ForeignKey(TTEventType, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)

    class Meta:
        unique_together = [('subject', 'event_type'),]
