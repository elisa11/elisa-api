# ./timetable/urls.py
from rest_framework.routers import DefaultRouter
from .views import TTViewSet, TTEventViewSet, AllowanceViewSet, TTEventControllerViewSet

from django.urls import include, path

router = DefaultRouter()
router.register(r'tt', TTViewSet, basename='tt' )
router.register(r'ttevent', TTEventViewSet, basename='ttevent' )
router.register(r'allowence', AllowanceViewSet, basename='allowence' )
router.register(r'ttecontroller', TTEventControllerViewSet, basename='ttecontroller' )

urlpatterns = router.urls
