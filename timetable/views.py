# ./timetable/views.py
from drf_spectacular.utils import OpenApiParameter, extend_schema
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from rest_framework.response import Response

from elisa.utils.LeveledSerializer import LeveledSerializerMixin
from timetable.services.processor import TTEventProcessor
from rest_framework.decorators import action

from elisa.utils.global_oapi import generic_django_ops_docs, term_schema_view
from elisa.utils.serializers import ok_response, err_response
from subjects.models import SubjectGroup
from .models import TT, TTEvent, Allowance
from timetable.services.serializers import TTSerializer, TTEventSerializer, AllowanceSerializer
from .services.filters import TTFilter, TTEventFilter, AllowanceFilter

@term_schema_view()
@generic_django_ops_docs
@extend_schema(tags=["TimeTable"])
class TTViewSet(LeveledSerializerMixin, viewsets.ModelViewSet):
    queryset = TT.objects.all()
    serializer_class = TTSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = TTFilter
    ordering_fields = ['id', 'name', 'term', 'status', 'created_at']
    ordering = ['id']

@term_schema_view()
@generic_django_ops_docs
@extend_schema(tags=["TimeTable Events"])
class TTEventViewSet(LeveledSerializerMixin, viewsets.ModelViewSet):
    queryset = TTEvent.objects.all()
    serializer_class = TTEventSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = TTEventFilter
    ordering_fields = ['id', 'day_of_week', 'start_time', 'duration']
    ordering = ['id']
    
    
@extend_schema(tags=["TimeTable Event controller"])
class TTEventControllerViewSet(viewsets.GenericViewSet):
    serializer_class = None
    @term_schema_view()
    @extend_schema(
        summary="Import FEI dataset from DATA directory",
        description="Import FEI exports from dir specified.",
        parameters=[
            OpenApiParameter(name='subjectgroup_name', description='Specifies the subjectgroup name', required=True, type=str),
            OpenApiParameter(name='tt_name', description='Specifies the tt name', required=True, type=str),
            OpenApiParameter(name='tt_program', description='Specifies the tt program', required=True, type=str),
        ],
        responses={200: TTEventSerializer(many=True)},
    )
    @action(detail=False, methods=['get'], url_path='generate-tte-events')
    def generate_tte_events(self, request):
        """
        Generate TTE events for subjectgroup groups
        """
        subjectgroup_name = request.query_params.get('subjectgroup_name')
        tt_name = request.query_params.get('tt_name')
        tt_program = request.query_params.get('tt_program')
        tt, _ = TT.objects.get_or_create(name=tt_name, program=tt_program)
        try:
            events = TTEventProcessor.generate_events(subjectgroup_name, tt)
            return Response(TTEventSerializer(events, many=True).data)
        except Exception as e:
            raise err_response(str(e), 400)


@term_schema_view()
@generic_django_ops_docs
@extend_schema(tags=["Allowance"])
class AllowanceViewSet(LeveledSerializerMixin, viewsets.ModelViewSet):
    queryset = Allowance.objects.all()
    serializer_class = AllowanceSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = AllowanceFilter
    ordering_fields = ['id', 'number']
    ordering = ['id']
