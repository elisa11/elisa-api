from django.db.models.manager import BaseManager

from subjects.models import SubjectGroup
from timetable.models import TT, TTEvent, Allowance
from timetable.ttutils import TTUtils


class TTEventProcessor:
    """
    Processor class for generating timetable events based on subject groups and their allowances.
    Handles the bulk creation of initial timetable events that can later be scheduled.
    """

    @staticmethod
    def generate_events(subjectgroup_name: str, tt: TT) -> list[TTEvent]:
        """
        Generates initial timetable events for all subjects in a subject group based on their allowances.
        
        Args:
            subjectgroup_name (str): Name of the subject group to generate events for
            tt (TT): Timetable object to associate the events with
            
        Returns:
            list[TTEvent]: List of created timetable events
            
        Raises:
            Exception: If the subject group is not found
            
        Note:
            - Created events have no day, time, or room assigned (these are set during scheduling)
            - Events are created with full-week availability (all weeks enabled in bitmask)
            - Duration is set based on the allowance amount
        """
        # Get the subject group and verify it exists
        subjectgroup = SubjectGroup.objects.filter(name=subjectgroup_name)
        if not subjectgroup:
            raise Exception("Subjectgroup not found")
        
        # Extract all subjects from the subject group in memory
        # This is more efficient than querying the subjects separately
        subjects = [sg.subject for sg in subjectgroup]
        
        # Fetch all allowances for these subjects in a single query
        # This reduces database hits compared to querying allowances per subject
        allowances = Allowance.objects.filter(subject__in=subjects)
        
        # Create event objects for each allowance
        # We pre-allocate the list for better performance
        all_events = [
            TTEvent(
                tt=tt,
                subject=allowance.subject,
                event_type=allowance.event_type,
                day_of_week=None,  # Will be set during scheduling
                start_time=None,   # Will be set during scheduling
                room=None,         # Will be set during scheduling
                duration=allowance.amount,
                weeks_bitmask=TTUtils.TTE_full_week_bitmask,  # Initially available all weeks
            )
            for allowance in allowances
        ]
            
        # Bulk create all events in a single database transaction
        if all_events:
            TTEvent.objects.bulk_create(all_events)
            
        return all_events
