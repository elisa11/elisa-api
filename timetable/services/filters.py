# ./timetable/services/filters.py
import django_filters
from ..models import TT, TTEvent, Allowance

class TTFilter(django_filters.FilterSet):
    """
    Filters for Timetable (TT) model:
    - name: String, case-insensitive search for timetable name
    - status: Choice field with options:
        * 'draft' - Initial timetable state
        * 'published' - Finalized and visible to users
        * 'archived' - Historical timetable
    - created_at: Date filter for creation timestamp (format: YYYY-MM-DD)
    - updated_at: Date filter for last update timestamp (format: YYYY-MM-DD)
    - program: String, regex-based search for study program code (e.g., 'BIT', 'MIT')
    """
    name = django_filters.CharFilter(
        lookup_expr='icontains',
        help_text='Case-insensitive search for timetable name'
    )
    status = django_filters.ChoiceFilter(
        choices=TT.STATUS_CHOICES,
        help_text='Filter by status: draft (initial), published (visible), archived (historical)'
    )
    created_at = django_filters.DateFilter(
        field_name='created_at',
        help_text='Filter by creation date (format: YYYY-MM-DD)'
    )
    updated_at = django_filters.DateFilter(
        field_name='updated_at',
        help_text='Filter by last update date (format: YYYY-MM-DD)'
    )
    program = django_filters.CharFilter(
        lookup_expr='iregex',
        help_text='Regex-based search for study program code (e.g., BIT, MIT)'
    )
    class Meta:
        model = TT
        fields = ['name', 'status', 'created_at', 'updated_at', 'program']

class TTEventFilter(django_filters.FilterSet):
    """
    Filters for Timetable Events (TTEvent) model:
    - tt: Number, filter by timetable ID
    - subject: Number, filter by subject ID
    - subject__code: String, regex search for subject code (e.g., 'API', 'AZA')
    - day_of_week: Number (0-6), where:
        * 0 = Monday
        * 1 = Tuesday
        * ...
        * 6 = Sunday
    - room: Number, filter by room ID
    - event_type: Number, filter by event type ID (lecture, lab, etc.)
    - start_time: Number (0-1439), minutes since midnight
        Examples: 
        * 540 = 09:00
        * 720 = 12:00
        * 960 = 16:00
    - duration: Number, event duration in minutes
    - weeks_bitmask: Number, binary mask for weeks when event occurs
        Example: 
        * 15 (binary: 1111) = event occurs in first 4 weeks
        * 255 (binary: 11111111) = event occurs in first 8 weeks
    """
    tt = django_filters.NumberFilter(
        field_name='tt__id',
        help_text='Filter by timetable ID'
    )
    subject = django_filters.NumberFilter(
        field_name='subject__id',
        help_text='Filter by subject ID'
    )
    subject__code = django_filters.CharFilter(
        field_name='subject__code',
        lookup_expr='iregex',
        help_text='Regex search for subject code (e.g., API, AZA)'
    )
    day_of_week = django_filters.NumberFilter(
        help_text='Day of week (0=Monday, 1=Tuesday, ..., 6=Sunday)'
    )
    room = django_filters.NumberFilter(
        field_name='room__id',
        help_text='Filter by room ID'
    )
    event_type = django_filters.NumberFilter(
        field_name='event_type__id',
        help_text='Filter by event type ID (lecture, lab, etc.)'
    )
    start_time = django_filters.NumberFilter(
        field_name='start_time',
        help_text='Time slot (e.g., 0=07:00, 1=8:30, 2=10:00)'
    )
    duration = django_filters.NumberFilter(
        field_name='duration',
        help_text='Event duration in number of time slots'
    )
    weeks_bitmask = django_filters.NumberFilter(
        field_name='weeks_bitmask',
        help_text='Binary mask for weeks (e.g., 15=first 4 weeks, 255=first 8 weeks)'
    )

    class Meta:
        model = TTEvent
        fields = ['subject', 'day_of_week', 'weeks_bitmask', 'room', 'event_type', 'start_time', 'duration']

class AllowanceFilter(django_filters.FilterSet):
    """
    Filters for Subject Allowances:
    - subject: Number, filter by subject ID
    - event_type: Number, filter by event type ID
        Examples of event types:
        * 1 = Lecture
        * 2 = Laboratory
        * 3 = Exercise
        * etc.
    
    Note: Allowances define how many hours/events of each type a subject should have
    Example: Subject "Mathematics" might have:
    - 2 hours of lectures (event_type=1)
    - 2 hours of exercises (event_type=3)
    """
    subject = django_filters.NumberFilter(
        field_name='subject__id',
        help_text='Filter by subject ID'
    )
    event_type = django_filters.NumberFilter(
        field_name='event_type__id',
        help_text='Filter by event type ID (1=Lecture, 2=Exercise, 2=Seminar, etc.)'
    )

    class Meta:
        model = Allowance
        fields = ['subject', 'event_type']
