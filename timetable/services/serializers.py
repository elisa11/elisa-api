# ./timetable/services/serializers.py
from rest_framework import serializers

from elisa.utils.LeveledSerializer import LeveledModelSerializer
from timetable.models import TT, TTEvent, Allowance, UserTtEvent

class TTSerializer(LeveledModelSerializer):
    class Meta:
        model = TT
        fields = ['id', 'name', 'program', 'owner', 'status', 'created_at', 'updated_at']


class TTEventSerializer(LeveledModelSerializer):
    class Meta:
        model = TTEvent
        fields = [
            'id', 'tt', 'subject', 'event_type', 'day_of_week',
            'start_time', 'duration', 'room', 'weeks_bitmask'
        ]
    # serialize the weeks_bitmask as a binary string
    weeks_bitmask = serializers.SerializerMethodField()
    def get_weeks_bitmask(self, obj):
        return bin(obj.weeks_bitmask)[2:]

class UserTtEventSerializer(LeveledModelSerializer):
    class Meta:
        model = UserTtEvent
        fields = ['user', 'tt_event', 'connection']

class AllowanceSerializer(LeveledModelSerializer):
    class Meta:
        model = Allowance
        fields = ['id', 'subject', 'event_type', 'amount']
