# ./users/services/oapi_user.py
from drf_spectacular.utils import OpenApiResponse, OpenApiExample
from datetime import datetime

from rest_framework.response import Response
from rest_framework import status

from users.models import User
from elisa.utils.utils import Err_serializer
from .serializers import UserSerializer


class UserResponse:
    # Define reusable OpenAPI response definitions for user-related views
    responses = {
        200: OpenApiResponse(
            response=UserSerializer,
            examples=[OpenApiExample("User Response",
                                     value={
                                            "id": 1,
                                            "name": "xskulsky",
                                            "fullname": "Matej Skulsky",
                                            "last_logout": datetime.now().isoformat()
                                        },
                                     description="Retrieved single user.")],
            description="Retrieved single user."
        ),
        404: OpenApiResponse(
            response=Err_serializer,
            examples=[OpenApiExample("Error Response",
                                     value={"message": "User not found",
                                            "timestamp": datetime.now().isoformat()},
                                     description="User not found")],
            description="User not found"
        )
    }

    @staticmethod
    def __getitem__(key):
        return UserResponse.responses.get(key, None)

    @staticmethod
    def make_response(user_instance):
        if isinstance(user_instance, User):
            serializer = UserSerializer(user_instance)
            return Response(serializer.data, status=status.HTTP_200_OK)
        elif isinstance(user_instance, int):
            return Response(UserResponse.responses[user_instance].examples[0].value, status=user_instance)
