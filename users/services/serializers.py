# ./users/services/serializers.py
from rest_framework import serializers

from elisa.utils.LeveledSerializer import LeveledModelSerializer
from ..models import User
from subjects.models import SubjectUserRole

class UserSerializer(LeveledModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'full_name', 'last_logout']

