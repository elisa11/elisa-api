# ./users/services/filters.py
import django_filters
from ..models import User
from subjects.models import SubjectUserRole

class UserFilter(django_filters.FilterSet):
    username = django_filters.CharFilter(lookup_expr='iregex')

    class Meta:
        model = User
        fields = ['username', 'full_name']

class SubjectUserRoleFilter(django_filters.FilterSet):
    user = django_filters.NumberFilter(field_name='user__id')
    subject = django_filters.NumberFilter(field_name='subject__id')
    name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = SubjectUserRole
        fields = ['user', 'subject', 'name']
