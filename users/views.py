# ./users/views.py
from drf_spectacular.utils import extend_schema
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter

from elisa.utils.LeveledSerializer import LeveledSerializerMixin
from elisa.utils.global_oapi import generic_django_ops_docs
from .models import User
from subjects.models import SubjectUserRole
from .services.serializers import UserSerializer
from .services.filters import UserFilter, SubjectUserRoleFilter

@generic_django_ops_docs
@extend_schema(tags=["User"])
class UserViewSet(LeveledSerializerMixin, viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = UserFilter
    ordering_fields = ['id', 'username', 'full_name']
    ordering = ['id']
