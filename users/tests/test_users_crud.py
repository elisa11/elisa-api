# ./users/tests/test_users_crud.py
from django.urls import reverse
from django.utils import timezone

from rest_framework import status

from users.models import User
from users.services.serializers import UserSerializer
import test.helpers


class UserCrudTestCase(test.helpers.TestBase):
    def setUp(self):
        self._user1 = User.objects.create(username="testuser1", full_name="Test User1", last_logout=timezone.now())
        self._user1_json = UserSerializer(self._user1).data

        self.urls = {
            "create": reverse('users-create-user'),
            "get": reverse('users-get-user') + '?id={}',
            "update": reverse('users-update-user') + '?id={}',
            "delete": reverse('users-delete-user') + '?id={}'
        }

    def create_user(self, data, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('post', self.urls['create'], data, exp_status_code, exp_message)

    def get_user(self, user_id, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('get', self.urls['get'], None, exp_status_code, exp_message, user_id)

    def update_user(self, user_id, data, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('put', self.urls['update'], data, exp_status_code, exp_message, user_id)

    def delete_user(self, user_id, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('delete', self.urls['delete'], None, exp_status_code, exp_message, user_id)

    def prepare_data(self, username=None, full_name=None, last_logout=None):
        user_json = UserSerializer(
            User(username=username, full_name=full_name, last_logout=last_logout)
        ).data

        return {key: value for key, value in user_json.items() if value is not None}

    # Test cases
    def test_create_user(self):
        user2_json = self.prepare_data("testuser2", "Test User2", timezone.now())

        res = self.create_user(user2_json, status.HTTP_201_CREATED)
        self.assert_response_data(res.data, user2_json)

        invalid_data = self.prepare_data(full_name="Test User2", last_logout=timezone.now())
        self.create_user(invalid_data, status.HTTP_400_BAD_REQUEST, "Invalid data provided.")

        # Trying to create a user which already exists (duplicate)
        self.create_user(user2_json, status.HTTP_400_BAD_REQUEST, "Invalid data provided.")

        self.assert_model_state(User, 2, [self._user1_json, user2_json])

    def test_get_user(self):
        res = self.get_user(self._user1_json['id'], status.HTTP_200_OK)
        self.assert_response_data(res.data, self._user1_json)

        self.get_user(0, status.HTTP_404_NOT_FOUND, "User not found.")

        self.get_user('', status.HTTP_400_BAD_REQUEST, "No id provided.")

        self.assert_model_state(User, 1, [self._user1_json])

    def test_update_user(self):
        update_data = self.prepare_data("testuser2", "Test User2", timezone.now())

        res = self.update_user(self._user1_json['id'], update_data, status.HTTP_200_OK)
        self.assert_response_data(res.data, update_data)
        self.assert_model_state(User, 1, [update_data])

        partial_update_data = self.prepare_data(full_name="Test User2 Updated", last_logout=timezone.now())

        exp_data_after_partial_update = update_data.copy()
        exp_data_after_partial_update.update(partial_update_data)

        res = self.update_user(self._user1_json['id'], partial_update_data, status.HTTP_200_OK)
        self.assert_response_data(res.data, exp_data_after_partial_update)
        self.assert_model_state(User, 1, [exp_data_after_partial_update])

        self.update_user('', None, status.HTTP_400_BAD_REQUEST, "No id provided.")

        self.update_user(0, update_data, status.HTTP_404_NOT_FOUND, "User not found.")

    def test_delete_user(self):
        self.delete_user(self._user1_json['id'], status.HTTP_204_NO_CONTENT)

        self.delete_user(self._user1_json['id'], status.HTTP_404_NOT_FOUND, "User not found.")

        self.delete_user('', status.HTTP_400_BAD_REQUEST, "No id provided.")

        self.assert_model_state(User, 0, [])
