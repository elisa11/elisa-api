# ./users/tests/test_users_relations.py
from django.urls import reverse
from django.utils import timezone

from rest_framework import status

from users.models import User
from subjects.models import Subject, SubjectUserRole
from subjects.services.serializers import SubjectSerializer
from users.services.serializers import SubjectUserRoleSerializer, UserSerializer
import test.helpers

class UserRelationsTestCase(test.helpers.TestBase):
    def setUp(self):
        self._user1 = User.objects.create(username="testuser", full_name="Test User1", last_logout=timezone.now())
        self._user1_json = UserSerializer(self._user1).data

        self._subject1 = Subject.objects.create(code="T-Test1", name="Test Subject1", nominal_semester=1)
        self._subject2 = Subject.objects.create(code="T-Test2", name="Test Subject2", nominal_semester=2)
        self._subject1_json = SubjectSerializer(self._subject1).data
        self._subject2_json = SubjectSerializer(self._subject2).data

        self._role1 = SubjectUserRole.objects.create(user=self._user1, subject=self._subject1, name="student")
        self._role1_json = SubjectUserRoleSerializer(self._role1).data

        self.urls = {
            "create": reverse('users-relations-assign-to-subject'),
            "get_roles_subjects_ids": reverse('users-relations-at-subjects') + '?id={}',
            "get_roles_subjects_codes": reverse('users-relations-at-subjects-codes') + '?id={}',
            "delete": reverse('users-relations-remove-from-subject')
        }

    def create_role(self, data, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('post', self.urls['create'], data, exp_status_code, exp_message)

    def get_role_subject_id(self, user_id, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('get', self.urls['get_roles_subjects_ids'], None, exp_status_code, exp_message, user_id)

    def get_role_subject_code(self, user_id, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('get', self.urls['get_roles_subjects_codes'], None, exp_status_code, exp_message, user_id)

    def delete_role(self, data, exp_status_code, exp_message=None):
        return self.exec_and_validate_req('delete', self.urls['delete'], data, exp_status_code, exp_message)

    def prepare_data(self, user=None, subject=None, name=None):
        user_json = SubjectUserRoleSerializer(
            SubjectUserRole(user=user, subject=subject, name=name)
        ).data

        return {key: value for key, value in user_json.items() if value is not None}

    # Test cases
    def test_create_role(self):
        role2_json = self.prepare_data(self._user1, self._subject2, "student")

        res = self.create_role(role2_json, status.HTTP_201_CREATED)
        self.assert_response_data(res.data, role2_json)

        invalid_data1 = self.prepare_data(subject=self._subject2, name="student")
        self.create_role(invalid_data1, status.HTTP_404_NOT_FOUND, "User not found.")

        invalid_data2 = self.prepare_data(user=self._user1, name="student")
        self.create_role(invalid_data2, status.HTTP_404_NOT_FOUND, "Subject not found.")

        invalid_data3 = self.prepare_data(name="student")
        self.create_role(invalid_data3, status.HTTP_404_NOT_FOUND, "User and Subject are not found.")

        # Trying to create a role which already exists (duplicate)
        self.create_role(role2_json, status.HTTP_400_BAD_REQUEST, "Invalid data provided.")

        self.assert_model_state(SubjectUserRole, 2, [self._role1_json, role2_json])

    def test_get_roles_subjects_ids(self):
        # Adding 1 extra role
        role2_json = self.prepare_data(self._user1, self._subject2, "student")
        self.create_role(role2_json, status.HTTP_201_CREATED)

        # Testing
        res = self.get_role_subject_id(self._user1_json['id'], status.HTTP_200_OK)

        exp_role1_json = self._role1_json.copy()
        del exp_role1_json['user']

        exp_role2_json = role2_json.copy()
        del exp_role2_json['user']

        self.assert_response_data(res.data, [exp_role1_json, exp_role2_json])

        self.get_role_subject_id(0, status.HTTP_404_NOT_FOUND, "No relations found for the given user.")

        self.get_role_subject_id('', status.HTTP_400_BAD_REQUEST, "No user id provided.")

        self.assert_model_state(SubjectUserRole, 2, [self._role1_json, role2_json])

    def test_get_roles_subjects_codes(self):
        # Adding 1 extra role
        role2_json = self.prepare_data(self._user1, self._subject2, "student")
        self.create_role(role2_json, status.HTTP_201_CREATED)

        # Testing
        res = self.get_role_subject_code(self._user1_json['id'], status.HTTP_200_OK)

        exp_role1_json = {
            "name": self._role1_json['name'],
            "subject_code": self._subject1.code
        }

        exp_role2_json = {
            "name": role2_json['name'],
            "subject_code": self._subject2.code
        }

        self.assert_response_data(res.data, [exp_role1_json, exp_role2_json])

        self.get_role_subject_id(0, status.HTTP_404_NOT_FOUND, "No relations found for the given user.")

        self.get_role_subject_id('', status.HTTP_400_BAD_REQUEST, "No user id provided.")

        self.assert_model_state(SubjectUserRole, 2, [self._role1_json, role2_json])

    def test_delete_role(self):
        self.delete_role(self._role1_json, status.HTTP_204_NO_CONTENT)

        invalid_data1 = self.prepare_data(subject=self._subject2, name="student")
        self.delete_role(invalid_data1, status.HTTP_400_BAD_REQUEST, "Invalid data provided.")

        invalid_data2 = self.prepare_data(user=self._user1, name="student")
        self.delete_role(invalid_data2, status.HTTP_400_BAD_REQUEST, "Invalid data provided.")

        invalid_data3 = self.prepare_data(name="student")
        self.delete_role(invalid_data3, status.HTTP_400_BAD_REQUEST, "Invalid data provided.")

        # Trying to delete a role which was already deleted (double delete)
        self.delete_role(self._role1_json, status.HTTP_404_NOT_FOUND, "SubjectUserRole not found.")

        self.assert_model_state(SubjectUserRole, 0, [])
