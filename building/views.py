from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from drf_spectacular.utils import extend_schema

from elisa.utils.LeveledSerializer import LeveledSerializerMixin
from elisa.utils.global_oapi import generic_django_ops_docs, term_schema_view
from .models import Building, UserBuildingRole
from building.services.serializers import BuildingSerializer, UserBuildingRoleSerializer
from building.services.filters import BuildingFilter, UserBuildingRoleFilter
@term_schema_view()
@generic_django_ops_docs
@extend_schema(tags=["Building"])
class BuildingViewSet(LeveledSerializerMixin, viewsets.ModelViewSet):
    queryset = Building.objects.all()
    serializer_class = BuildingSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = BuildingFilter
    ordering_fields = ['id', 'name', 'abbrev']
    ordering = ['id']

@term_schema_view()
@generic_django_ops_docs
@extend_schema(tags=["UserBuildingRole"])
class UserBuildingRoleViewSet(LeveledSerializerMixin, viewsets.ModelViewSet):
    queryset = UserBuildingRole.objects.all()
    serializer_class = UserBuildingRoleSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = UserBuildingRoleFilter
    ordering_fields = ['user', 'department', 'name']
    ordering = ['user']
