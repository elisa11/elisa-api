from rest_framework.routers import DefaultRouter

from building.models import Building
from building.views import BuildingViewSet

router = DefaultRouter()
router.register(r'buildings', BuildingViewSet, basename='buildings')

urlpatterns = router.urls