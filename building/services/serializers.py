from rest_framework import serializers
from building.models import Building, UserBuildingRole
from elisa.utils.LeveledSerializer import LeveledModelSerializer


class BuildingSerializer(LeveledModelSerializer):
    class Meta:
        model = Building
        fields = ['id', 'name', 'abbrev']

class UserBuildingRoleSerializer(LeveledModelSerializer):
    class Meta:
        model = UserBuildingRole
        fields = ['user', 'department', 'name']
