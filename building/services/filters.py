import django_filters
from building.models import Building, UserBuildingRole

class BuildingFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='iregex')
    abbrev = django_filters.CharFilter(lookup_expr='iregex')

    class Meta:
        model = Building
        fields = ['name', 'abbrev']

class UserBuildingRoleFilter(django_filters.FilterSet):
    user = django_filters.NumberFilter(field_name='user__id')
    department = django_filters.NumberFilter(field_name='department__id')
    name = django_filters.CharFilter(lookup_expr='iregex')

    class Meta:
        model = UserBuildingRole
        fields = ['user', 'department', 'name']
