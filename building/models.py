from django.db import models

from users.models import User


class Building(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    abbrev = models.CharField(max_length=25)

    def __str__(self):
        return self.abbrev
    class Meta:
        db_table = 'elisa_building'

class UserBuildingRole(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    department = models.ForeignKey(Building, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)

    class Meta:
        unique_together = ('user', 'department')