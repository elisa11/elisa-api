from functools import wraps
from django.utils.decorators import method_decorator

def public_view(view_func):
    """Marks a view as public, bypassing tenant requirements"""
    view_func.is_public = True
    return view_func

def public_endpoint(view_class):
    """Class decorator to mark all view methods as public"""
    for method in ['get', 'post', 'put', 'patch', 'delete']:
        if hasattr(view_class, method):
            setattr(view_class, method, public_view(getattr(view_class, method)))
    return view_class 