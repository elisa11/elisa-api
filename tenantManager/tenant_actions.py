from abc import ABC, abstractmethod
from django.utils.module_loading import autodiscover_modules
from typing import ClassVar, List

class CustomTenantActions(ABC):
    """Base class for tenant actions that should be executed after schema operations"""
    _registry: ClassVar[List['CustomTenantActions']] = []

    def __init_subclass__(cls, **kwargs):
        """Automatically register any class that inherits from CustomTenantActions"""
        super().__init_subclass__(**kwargs)
        cls._registry.append(cls())

    @abstractmethod
    def post_create(self, schema_name: str) -> None:
        """Execute after schema creation"""
        pass

    @classmethod
    def execute_all_post_create(cls, schema_name: str) -> None:
        """Execute post_create for all registered actions"""
        cls.discover()
        for action in cls._registry:
            action.post_create(schema_name)

    @classmethod
    def discover(cls):
        """Autodiscover tenant_actions.py modules"""
        autodiscover_modules('tenant_actions') 