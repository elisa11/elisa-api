from drf_spectacular.openapi import SchemaGenerator
from django.urls import URLPattern, URLResolver
from django.urls import get_resolver
from elisa.urls import tenant_patterns 

class TenantSchemaGenerator(SchemaGenerator):
    def get_schema(self, *args, **kwargs):
        # Get the public schema
        schema = super().get_schema(*args, **kwargs)
        
        # Add tenant-specific paths
        tenant_urlconf = get_resolver(tenant_patterns)
        tenant_patterns = self.get_all_patterns(tenant_urlconf)
        
        # Process tenant patterns and add them to schema
        for path, path_regex, pattern in tenant_patterns:
            if hasattr(pattern, 'callback'):
                operations = self.get_operation(path, path_regex, pattern)
                if operations:
                    for method, operation in operations.items():
                        # Add tenant header parameter
                        operation.setdefault('parameters', []).append({
                            'name': 'X-schema',
                            'in': 'header',
                            'required': True,
                            'schema': {'type': 'string'},
                            'description': 'schema name (e.g., zs_2023_2024)'
                        })
                        schema.setdefault('paths', {}).setdefault(path, {})[method] = operation

        return schema

    def get_all_patterns(self, urlconf):
        patterns = []
        for pattern in urlconf.url_patterns:
            if isinstance(pattern, URLPattern):
                patterns.append(self.get_pattern_data(pattern))
            elif isinstance(pattern, URLResolver):
                patterns.extend(self.get_all_patterns(pattern))
        return patterns 