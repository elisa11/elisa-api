# ./tenantManager/apps.py
from django.apps import AppConfig


class TenantManagerConfig(AppConfig):
    name = 'tenantManager'
