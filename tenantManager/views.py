from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from elisa.utils.serializers import err_response, ok_response
from .models import Schema, Domain
from .serializers import schemaSerializer
from django.db import connection
from django_tenants.utils import schema_context
from drf_spectacular.utils import extend_schema, OpenApiParameter
from elisa.utils.global_oapi import generic_django_ops_docs
@generic_django_ops_docs
@extend_schema(tags=["Schema"])
class SchemaViewSet(viewsets.ModelViewSet):
    queryset = Schema.objects.all()
    serializer_class = schemaSerializer

    @extend_schema(
        parameters=None,
        request=None,
        responses={200: schemaSerializer}
    )
    @action(detail=True, methods=['post'])
    def activate(self, request, pk=None):
        """Explicit endpoint to activate a schema"""
        schema = self.get_object()
        Schema.set_active(schema.id)
        serializer = self.get_serializer(schema)
        return Response(serializer.data)
    
    @extend_schema(
        parameters=[
            OpenApiParameter(
                name='force',
                type=bool,
                description='If true, deactivates and deletes an active schema. If false, prevents deletion of active schemas.',
                required=False
            )])
    def destroy(self, request, *args, **kwargs):
        with schema_context('public'):
            schema = self.get_object()
            # You might want to add safety checks here
            # e.g., prevent deletion of active schemas
            if schema.is_active:
                force = request.query_params.get('force', '').lower() == 'true'  # if force is true, deactivate and delete
                if force:
                    schema.is_active = False
                    schema.save()
                    return ok_response("Cannot delete active schema. Use ?force=true to deactivate and delete."),
                else:
                    return err_response("Cannot delete active schema. Use ?force=true to deactivate and delete.",
                                        status.HTTP_400_BAD_REQUEST)
            t = super().destroy(request, *args, **kwargs)
            return t