from django.db import connection, models, transaction
from django_tenants.models import TenantMixin, DomainMixin
from django_tenants.utils import schema_context
from .tenant_actions import CustomTenantActions

class Domain(DomainMixin):
    pass 
class Schema(TenantMixin):
    human_name = models.CharField(max_length=20)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    is_active = models.BooleanField(default=False)
    schema_name = models.CharField(max_length=63, unique=True)
    
    auto_create_schema = True
    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['is_active'],
                condition=models.Q(is_active=True),
                name='unique_active_schema'
            )
        ]
    @classmethod
    def set_active(cls, schema_id):
        """Helper method to activate a schema and deactivate others"""
        with transaction.atomic():
            # Deactivate all schemas
            cls.objects.all().update(is_active=False)
        with transaction.atomic():
            # Activate the selected schema
            cls.objects.filter(id=schema_id).update(is_active=True)
        
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._original_schema_name = self.schema_name if self.pk else None

    def __str__(self):
        return self.human_name

    def save(self, *args, **kwargs):
        is_new = self.pk is None
        if not self.schema_name:
            # Convert "ZS 2023/2024" to "zs_2023_2024"
            self.schema_name = self.human_name.lower().replace(' ', '_').replace('/', '_')
        with schema_context('public'):
            if self.is_active:
                Schema.objects.exclude(pk=self.pk).update(is_active=False)
            super().save(*args, **kwargs)

            if is_new and not Domain.objects.filter(domain='localhost', is_primary=True).exists():
                Domain.objects.create(
                    domain='localhost',
                    tenant=self,
                    is_primary=True
                )

    def create_schema(self, check_if_exists=False, sync_schema=True, verbosity=1):
        """Override to add post-creation actions after schema creation"""
        # Call parent method first
        super().create_schema(check_if_exists=check_if_exists, sync_schema=sync_schema, verbosity=verbosity)
        
        # Execute all registered post-create actions
        CustomTenantActions.execute_all_post_create(self.schema_name)
