from django_tenants.middleware import TenantMainMiddleware
from django_tenants.utils import get_tenant_model, get_public_schema_name
from django.http import Http404
from django.db import connection
from django.contrib.contenttypes.models import ContentType
from django.urls import resolve, Resolver404
from drf_spectacular.utils import extend_schema

from elisa.utils.serializers import err_response

class TenantNotFound(Exception):
    def __init__(self, message, status_code=404):
        self.message = message
        self.status_code = status_code


class schemaMiddleware(TenantMainMiddleware):
    """
    Determines tenant by the value of the 'X-Term' HTTP header.
    If not specified, falls back to the active schema.
    Public routes are always accessible.
    """

    PUBLIC_PATHS = [
        '/api/auth/',          # Authentication endpoints
        '/api/token/',         # JWT tokens
        '/api/terms/',         # Term management
        '/api/schemas/',        # API documentation
        '/admin/',             # Admin interface
        '/public/',  # Admin interface
    ]

    def is_public_path(self, path):
        # First check the view decorator
        view_func = self.get_view_func(path)
        if view_func and getattr(view_func, 'is_public', False):
            return True
            
        # Then check static paths
        if path.startswith('/api/schema/'):
            return True
        return any(path.startswith(public_path) for public_path in self.PUBLIC_PATHS)

    def get_view_func(self, path):
        try:
            resolved = resolve(path)
            return resolved.func
        except Resolver404:
            return None

    def get_public_name(self):
        try:
            schema = get_tenant_model().objects.filter(is_active=True).last()
            return schema.schema_name if schema else get_public_schema_name()
        except (get_tenant_model().DoesNotExist, AttributeError):
            return get_public_schema_name()

    def process_request(self, request):
        # Always start with public schema
        connection.set_schema_to_public()
        
        # Allow public paths without tenant
        if self.is_public_path(request.path):
            return

        # Get schema name from header, fall back to active schema
        schema_name = request.headers.get('X-Term',None)
        if not schema_name:
            return err_response("X-Term required")

        try:
            # Get the schema and set it as tenant
            schema = get_tenant_model().objects.get(schema_name=schema_name)
            request.tenant = schema
            connection.set_tenant(request.tenant)
            # Clear the content type cache
            ContentType.objects.clear_cache()
            
        except get_tenant_model().DoesNotExist:
            if schema_name != get_public_schema_name():
                return err_response(f"Term '{schema_name}' not found")

    def process_response(self, request, response):
        """
        Reset schema to public after request is processed
        """
        connection.set_schema_to_public()
        return response