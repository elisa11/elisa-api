from rest_framework import serializers
from .models import Schema, Domain
from django_tenants.utils import schema_context

class schemaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schema
        fields = ['id', 'human_name','schema_name', 'start_date', 'end_date', 'is_active']

        # Default values for fields if not provided
        default_fields = {
            'is_active': False,
        }
        # optional fields
        optional_fields = ['schema_name', 'start_date', 'end_date', 'is_active']
        # read only fields
        read_only_fields = ['id', 'schema_name']
        # write only fields
        write_only_fields = ['is_active']
