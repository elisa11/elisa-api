from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import SchemaViewSet

router = DefaultRouter()
router.register('schemas', SchemaViewSet)

urlpatterns = [
    path('', include(router.urls)),
]