# Design UML README

This document explains how to generate and work with UML diagrams
in this repository. The UML diagrams are created using PlantUML.

## Prerequisites

### Requirements
- Java installed on your system (required to run PlantUML).
- `plantuml.jar` (included in this repository) for generating diagrams. [license](#plantuml-license)

## Diagram
see [template.puml](template.puml).
The DEFINE `FILENAME`

## Generating UML Diagrams

### Directory Structure
Ensure your `.puml` files are placed in this directory or children.
1. Run `make`. (default target `make svg`)
2. Diagrams will be created at .puml file.

### make targets
All targets inject variable FILENAME as relative path to file without
.puml extension. File on path UC/user produces FILENAME "UC/user"
* `make svg`: generates diagrams in svg
* `make png`: generates diagrams in png
* `make svg-notes`: same as svg, but defines directive SHOW_NOTES
* `make png-notes`: same as png, but defines directive SHOW_NOTES


### Generating UML Diagrams manually

Run the following commands to generate diagrams:

#### Generate SVG Diagrams for All `.puml` Files
```bash
java -jar plantuml.jar -tsvg -DDEFINE=SHOW_NOTES -DFILENAME="UC/user" UC/user.puml;
java -jar plantuml.jar -tsvg -DDEFINE=SHOW_NOTES -DFILENAME="UC/user" UC/user.puml;
java -jar UML/plantuml.jar -tsvg UML/*.puml

```
## Understanding the Diagrams

### Diagram Types in This Repository
1. **Sequence Diagrams**: Represent the flow of operations between actors and systems.
2. **Use Case Diagrams**: Depict interactions between actors and system features.


## Licensing

This repository includes `plantuml.jar`, which is licensed under the GNU General Public License (GPL). Ensure compliance with the GPL when redistributing this repository or using PlantUML.

### PlantUML License
PlantUML is distributed under the [GPL License](https://www.gnu.org/licenses/gpl-3.0.html).
Redistribution of this file must comply with its license, but your generated diagrams are your own and not bound by the GPL.

### Common Errors
- **`java` not found**: Ensure Java is installed and added to your `PATH`.
- **File not found**: Check the directory structure and file paths.

### Support
If you encounter issues, consult the [PlantUML documentation](https://plantuml.com/) for further assistance.