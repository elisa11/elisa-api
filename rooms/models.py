# ./rooms/models.py
from django.db import models

from building.models import Building


class Equipment(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Room(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    capacity = models.IntegerField()
    building = models.ForeignKey(Building, blank=True, null=True, on_delete=models.SET_NULL)
    equipment = models.ManyToManyField(Equipment, through='RoomEquipment')
    # room types not necessary
    # #room_types = models.ManyToManyField(RoomType, through='RoomTypeRoom')

    def __str__(self):
        return self.name
    class Meta:
        unique_together = [('name', 'building'),]

class RoomEquipment(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    equipment = models.ForeignKey(Equipment, on_delete=models.CASCADE)
    count = models.IntegerField()

    class Meta:
        unique_together = [('room', 'equipment'),]

class RoomGroup(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class RoomGroup_Room(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    room_group = models.ForeignKey(RoomGroup, on_delete=models.CASCADE)

    class Meta:
        unique_together = [('room', 'room_group'),]