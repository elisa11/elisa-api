# ./rooms/services/serializers.py
from rest_framework import serializers

from elisa.utils.LeveledSerializer import LeveledModelSerializer
from rooms.models import Room, Equipment, RoomEquipment

class RoomSerializer(LeveledModelSerializer):
    class Meta:
        model = Room
        fields = ['id', 'name', 'capacity', 'building']

class EquipmentSerializer(LeveledModelSerializer):
    class Meta:
        model = Equipment
        fields = ['id', 'name']

class RoomEquipmentSerializer(LeveledModelSerializer):
    class Meta:
        model = RoomEquipment
        fields = ['room', 'equipment', 'count']
