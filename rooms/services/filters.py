# ./rooms/services/filters.py
import django_filters
from rooms.models import Room, Equipment, RoomEquipment


class RoomFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='iregex')
    capacity = django_filters.NumberFilter(lookup_expr='gte')
    building = django_filters.NumberFilter(field_name='building__id')

    class Meta:
        model = Room
        fields = ['name', 'capacity', 'building']

class EquipmentFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='iregex')

    class Meta:
        model = Equipment
        fields = ['name']

class RoomEquipmentFilter(django_filters.FilterSet):
    room = django_filters.CharFilter(lookup_expr='iregex')

    class Meta:
        model = RoomEquipment
        fields =  ['room', 'equipment', 'count']