# ./rooms/views.py
from drf_spectacular.utils import extend_schema
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter

from elisa.utils.LeveledSerializer import LeveledSerializerMixin
from elisa.utils.global_oapi import generic_django_ops_docs, term_schema_view
from rooms.services.filters import EquipmentFilter, RoomEquipmentFilter, RoomFilter
from rooms.models import Equipment, Room, RoomEquipment
from rooms.services.serializers import EquipmentSerializer, RoomEquipmentSerializer, RoomSerializer

@term_schema_view()
@generic_django_ops_docs
@extend_schema(tags=["Rooms"])
class RoomViewSet(LeveledSerializerMixin, viewsets.ModelViewSet):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = RoomFilter
    ordering_fields = ['id', 'name', 'capacity']
    ordering = ['id']

@term_schema_view()
@generic_django_ops_docs
@extend_schema(tags=["Equipment"])
class EquipmentViewSet(LeveledSerializerMixin, viewsets.ModelViewSet):
    queryset = Equipment.objects.all()
    serializer_class = EquipmentSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = EquipmentFilter
    ordering_fields = ['id', 'name']
    ordering = ['id']

@term_schema_view()
@generic_django_ops_docs
@extend_schema(tags=["Room Equipment"])
class RoomEquipmentViewSet(LeveledSerializerMixin, viewsets.ModelViewSet):
    queryset = RoomEquipment.objects.all()
    serializer_class = RoomEquipmentSerializer
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_class = RoomEquipmentFilter
    ordering_fields = ['id', 'room', 'equipment']
    ordering = ['id']
