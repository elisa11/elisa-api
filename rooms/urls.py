# ./rooms/urls.py
from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import EquipmentViewSet, RoomEquipmentViewSet
from .views import RoomViewSet

router = DefaultRouter()
router.register(r'rooms', RoomViewSet)
router.register(r'equipment', EquipmentViewSet)
router.register(r'room-equipment', RoomEquipmentViewSet)
urlpatterns = router.urls