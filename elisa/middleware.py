# myapp/middleware.py
from datetime import datetime

from django.db import connection
import logging

from django.http import JsonResponse

from django.conf import settings

logger = logging.getLogger('elisa')


class LogEndpointUsageMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        # Log the request and response status code
        logger.info(f"Endpoint: {request.path}, Method: {request.method}, Status: {response.status_code}")

        return response


def json_err_response(status_code: int, message: str):
    return JsonResponse(
        {
            "code": status_code,
            "message": message,
            "timestamp": datetime.now().isoformat()
        },
        status=status_code
    )

