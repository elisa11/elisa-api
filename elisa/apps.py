# elisa/apps.py
from django.apps import AppConfig
from django.db import OperationalError, ProgrammingError
from django.db.models.signals import post_init, post_migrate


class ElisaConfig(AppConfig):
    name = 'elisa'
