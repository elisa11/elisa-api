from django.core.management.base import BaseCommand
from django.core.management import call_command
from elisa.settings import INSTALLED_APPS, enableMigrations

class Command(BaseCommand):
    help = 'Make migrations and migrate for all ELISA apps'

    def handle(self, *args, **kwargs):
        enableMigrations(True)
        
        # First make migrations for all apps
        for app in INSTALLED_APPS:
            self.stdout.write(f"Making migrations for app: {app}")
            call_command('makemigrations', app.split('.')[-1])
        
        # Then migrate all apps
        self.stdout.write("\nApplying migrations for all apps...")
        call_command('migrate')
        
        enableMigrations(False)
        self.stdout.write(self.style.SUCCESS('Successfully made and applied all migrations')) 