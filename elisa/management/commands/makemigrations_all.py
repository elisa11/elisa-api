
from django.core.management.base import BaseCommand
from django.core.management import call_command
from elisa.settings import INSTALLED_APPS,enableMigrations

class Command(BaseCommand):
    help = 'Make migrations for all ELISA apps'
    def handle(self, *args, **kwargs):
        enableMigrations(True)
        for app in INSTALLED_APPS:
            self.stdout.write(f"Making migrations for app: {app}")
            call_command('makemigrations',
                         app.split('.')[-1]) # name of app is just last word
        enableMigrations(False)
