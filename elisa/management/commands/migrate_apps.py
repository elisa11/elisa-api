import logging

from django.core.management import BaseCommand, call_command
from django.conf import settings
from elisa.settings import enableMigrations

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Make migrations for all apps defined in a list in settings.'

    def add_arguments(self, parser):
        # Optional argument for creating new schema metadata
        parser.add_argument(
            '--VARLIST',
            type=str,
            help='Specify the list variable in elisa.settings to migrate e.g. static_apps',
        )

    def handle(self, *args, **options):
        apps_to_migrate = getattr(settings, options['VARLIST'], None)
        enableMigrations(True)
        if not apps_to_migrate:
            self.stdout.write(self.style.WARNING(f"No apps to migrate found in settings.{options['VARLIST']}"))
        apps_to_migrate = [ app.split('.')[-1] for app in apps_to_migrate]
        successful_apps = []
        failed_apps = []

        # Run makemigrations for each app separately, continue even if one fails
        for app in apps_to_migrate:
            try:
                logger.log(logging.INFO, f"Making migrations for app: {app}...")
                call_command('makemigrations', app)
                successful_apps.append(app)
                logger.log(logging.INFO, f"Successfully made migrations for app: {app}")
            except Exception as e:
                failed_apps.append(app)
                logger.log(logging.ERROR, f"Failed to make migrations for app: {app}. Error: {str(e)}")

        # Run migrate for each app separately, continue even if one fails
        for app in successful_apps:
            try:
                logger.log(logging.INFO,f'Applying migrations for app: {app}...')
                call_command('migrate', app)
                logger.log(logging.INFO,f"Successfully applied migrations for app: {app}")
            except Exception as e:
                failed_apps.append(app)
                logger.log(logging.ERROR,f"Failed to apply migrations for app: {app}. Error: {str(e)}")

        # Summary
        if successful_apps:
            logger.log(logging.INFO,f"Successfully migrated apps: {', '.join(successful_apps)}")
        if failed_apps:
            logger.log(logging.WARNING, f"Some apps failed to migrate: {', '.join(failed_apps)}")
        enableMigrations(True)
