import time
import psycopg2
from django.core.management.base import BaseCommand
from django.db import connections
from django.db.utils import OperationalError

class Command(BaseCommand):
    help = 'Wait for the database to be available'

    def handle(self, *args, **options):
        self.stdout.write('Waiting for database...')
        db_conn = None
        retries = 15
        while not db_conn:
            try:
                db_conn = connections['default']
                db_conn.cursor()
            except OperationalError:
                retries -= 1
                if retries == 0:
                    self.stderr.write('Database unavailable, exiting after max retries')
                    raise
                self.stdout.write('Database unavailable, waiting 2 seconds...')
                time.sleep(2)
        if db_conn:
            self.stdout.write(self.style.SUCCESS('Database available!'))
        else:
            self.stderr.write('Database unavailable, exiting after max retries')
