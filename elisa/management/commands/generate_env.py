import os
import re
from pathlib import Path
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    help = "Generate a .env file based on os.getenv calls in the project."

    def handle(self, *args, **kwargs):
        # Define the base directory of your project
        base_dir = Path(__file__).resolve().parent.parent.parent.parent

        # Define a pattern to search for os.getenv('KEY', 'default') calls
        pattern = re.compile(r"os\.getenv\(\s*['\"]([A-Z_]+)['\"]\s*,\s*['\"]([^'\"]*)['\"]\s*\)")

        # Initialize an empty string to store env file contents

        found_calls = dict() # file: {key : val}
        # Walk through all the Python files in the project
        for root, dirs, files in os.walk(base_dir):
            for file in files:
                if file.endswith('.py'):
                    file_path = os.path.join(root, file)
                    with open(file_path, 'r', encoding='utf-8') as f:
                        content = f.read()

                        # Search for os.getenv patterns
                        matches = pattern.findall(content)
                        if matches:
                            relative_path = Path(file_path).relative_to(base_dir)
                            for key, default_value in matches:
                                if not found_calls.get(relative_path):
                                    found_calls[relative_path] = dict()
                                found_calls[relative_path][key] = default_value

        # Write the final content to .env file
        env_file_path = base_dir / "EXPORTED.env"
        with open(env_file_path, 'w', encoding='utf-8') as env_file:
            env_file.write(f"#REGENERATE THIS FILE WITH: python manage.py {__name__.split('.')[-1]}\n"
                           f"#RENAME THIS TO JUST .env\n")

            for file, pairs in found_calls.items():
                env_file.write(f"\n\n###### {file} #####\n")

                for key, value in pairs.items():
                    env_file.write(f'{key}="{value}"\n')



        self.stdout.write(self.style.SUCCESS(f"Generated .env file at {env_file_path}"))
