
from django.core.management.base import BaseCommand
import shutil
from elisa.settings import ELISA_APPS
import os

class Command(BaseCommand):
    help = 'Remove migration files for all ELISA apps'

    def handle(self, *args, **kwargs):
        for app in ELISA_APPS:
            app_parts = app.split('.')
            app_path = os.path.join(*app_parts, 'migrations')
            if os.path.exists(app_path):
                self.stdout.write(f"Removing ({app_path}) migrations for app: {app}")
                shutil.rmtree(app_path)
            else:
                self.stdout.write(f"No migrations directory found for app: {app}")

        self.stdout.write(self.style.SUCCESS('Migrations successfully removed!'))



