# make_schema.py

import logging
from django.core.management import BaseCommand, call_command
from django.db import connection, ProgrammingError
from elisa.models import Schemas_metadata
from django.conf import settings
from elisa.settings import enableMigrations

logger = logging.getLogger(__name__)


def create_dynamic_migrations(schema_name):
    from django.conf import settings
    from django.apps import apps

    dynamic_apps = getattr(settings, 'DYNAMIC_APPS', [])
    dynamic_apps = [app.split('.')[-1] for app in dynamic_apps]  # Extract app names without namespaces

    with connection.cursor() as cursor:
        cursor.execute(f"SET search_path TO {schema_name};")
        cursor.execute("""
            CREATE TABLE IF NOT EXISTS django_migrations AS
            TABLE public.django_migrations WITH DATA;
        """)
        # Delete records that do belong to dynamic apps
        query = f"""
            DELETE FROM django_migrations
            WHERE app IN ({', '.join(['%s'] * len(dynamic_apps))});
        """
        cursor.execute(query, dynamic_apps)
        cursor.execute("SET search_path TO public;")


class Command(BaseCommand):
    help = 'Create a new schema and apply existing migrations for dynamic apps.'

    def add_arguments(self, parser):
        parser.add_argument(
            '--create-schema',
            type=str,
            help='Specify the schema name to create and add metadata'
        )
        parser.add_argument(
            '--no-metadata',
            action='store_true',
            help='Make the schema without metadata'
        )

    def handle(self, *args, **options):
        # 1: Extract schema name from command arguments
        schema_name = options.get('create_schema')
        make_metadata = options.get('no-metadata')
        if not schema_name:
            self.stderr.write(self.style.ERROR("Schema name is required. Use --create-schema <schema_name>."))
            return

        self.stdout.write(f'Creating schema "{schema_name}"...')

        # 2: Create the schema if not exists
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"CREATE SCHEMA IF NOT EXISTS {schema_name};")
            self.stdout.write(self.style.SUCCESS(f'Schema "{schema_name}" created successfully.'))
        except ProgrammingError as e:
            self.stderr.write(self.style.ERROR(f"Failed to create schema: {e}"))
            return

        # 3: Set the search path to the new schema and apply migrations
        try:
            enableMigrations(True)
            create_dynamic_migrations(schema_name)
            apply_migrations_for_schema(schema_name, no_metadata=make_metadata)
            enableMigrations(False)

        except Exception as e:
            self.stderr.write(self.style.ERROR(f"Failed to set search path or apply migrations: {e}"))

def apply_migrations_for_schema(schema_name, no_metadata=False):
    apps_to_migrate = settings.DYNAMIC_APPS
    successful_apps = []
    failed_apps = []

    # Apply migrations for all apps in DYNAMIC_APPS
    with connection.cursor() as cursor:
        cursor.execute(f"SET search_path TO {schema_name}, public")

    for app in apps_to_migrate:
        app_name = app.split('.')[-1]
        try:
            logger.log(logging.INFO, f"Migrations for app: {app_name} in schema '{schema_name}'...")
            call_command('makemigrations', app_name)
            successful_apps.append(app_name)
            logger.log(logging.INFO, f"Successfully migrated app: {app_name} in schema '{schema_name}'.")
        except Exception as e:
            failed_apps.append(app_name)
            logger.log(logging.ERROR, f"Failed to migrate app: {app_name}. Error: {e}")
    call_command('migrate')

    try:
        if not no_metadata:
            Schemas_metadata.objects.get_or_create(schemas_name=schema_name)
    except Exception as e:
        logger.log(logging.ERROR, f"Failed to create schema metadata: {e}")
        return
    if successful_apps:
        logger.log(logging.INFO, f"Successfully migrated apps: {', '.join(successful_apps)}")
    if failed_apps:
        logger.log(logging.WARNING, f"Some apps failed to migrate: {', '.join(failed_apps)}")