"""Elisa URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView
from rest_framework.routers import DefaultRouter
from django_tenants.urlresolvers import tenant_patterns

# Public URLs (non-tenant specific)
router = DefaultRouter()

public_patterns = [
    path('admin/', admin.site.urls),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/', include('users.urls')),
    path('api/', include('tenantManager.urls')),

    # OpenAPI schema and Swagger docs
    path('public/spec/', SpectacularAPIView.as_view(), name='schema'),
    path('public/spec/swagger/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    path('public/spec/redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),
]

# Tenant-specific URLs (these will be under /api/{semester_name}/)
tenant_patterns = [
    path('api/', include('subjects.urls')),
    path('api/', include('rooms.urls')),
    path('api/', include('timetable.urls')),
   #path('api/', include('reservation.urls')),
    path('api/', include('imports_exports.urls')),
    path('api/', include('building.urls')),
]

# Main URL patterns
urlpatterns = public_patterns + tenant_patterns