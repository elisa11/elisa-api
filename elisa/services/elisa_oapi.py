from drf_spectacular.utils import extend_schema, extend_schema_view

elisa_management_schema = extend_schema_view(
    list = extend_schema(
        summary="Retrieve information about a specific schema",
        description="Retrieves schema info. If param not set, gets all available schemas",
    ),
    create = extend_schema(
        summary="Create a new schema for a specified term",
        description="Input name is normalised regex.substitute('\W','_') to db table name like `term: ws 2024/2025` term_ws_2024_2025",
    )
)