from rest_framework import serializers
from elisa.models import Schemas_metadata


class Schemas_metadataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schemas_metadata
        fields = '__all__'
