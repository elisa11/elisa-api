import django_filters

from elisa.models import Schemas_metadata


class SchemasFilter(django_filters.FilterSet):
    id = django_filters.NumberFilter(field_name='id')
    name = django_filters.CharFilter(field_name='name')
    created_at = django_filters.DateFilter(lookup_expr='created_at')

    class Meta:
        model = Schemas_metadata
        fields = '__all__'
