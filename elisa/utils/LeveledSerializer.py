import sys

from drf_spectacular.utils import OpenApiParameter, extend_schema, \
    extend_schema_view
from rest_framework import serializers


class LeveledSerializerMixin:
    """
    Mixin that adds support for nested serialization depth through query parameters.
    Add this to any ViewSet that uses LeveledModelSerializers.
    """

    def __init_subclass__(cls, **kwargs):
        """Automatically apply schema extension when mixin is used"""
        super().__init_subclass__(**kwargs)
        return extend_schema_view(
            list=cls.extend_schema_with_level(),
            retrieve=cls.extend_schema_with_level(),
        )(cls)

    @property
    def level_param(self):
        try:
            return max(int(self.request.query_params.get('max-level', 1)), 0)
        except (ValueError, TypeError):
            return 1

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['level'] = self.level_param
        return context

    @classmethod
    def extend_schema_with_level(cls, **kwargs):
        return extend_schema(
            parameters=[
                OpenApiParameter(
                    name='max-level',
                    type=int,
                    location=OpenApiParameter.QUERY,
                    description='Maximum nesting level for serialized objects.'
                                'Use with causion of circular references',
                    default=1,
                    required=False,
                )
            ],
            **kwargs
        )

class LeveledModelSerializer(serializers.ModelSerializer):
    """Base serializer that automatically handles nested serialization"""
    
    def get_fields(self):
        """
        Override get_fields to handle automatic field conversion.
        This method is called by DRF when building the serializer fields.
        """
        # Get the original fields from ModelSerializer
        fields = super().get_fields()
        
        # Debug #print to see what fields we're starting with
        #print(f"Initial fields for {self.__class__.__name__}:", fields)
        
        # Examine each field in the serializer
        for field_name, field in fields.items():
            #print(f"Processing field: {field_name}, type: {type(field)}")
            
            # Check if this is a PrimaryKeyRelatedField (foreign key)
            if isinstance(field, serializers.PrimaryKeyRelatedField):
                #print(f"Found PrimaryKeyRelatedField: {field_name}")
                
                # Get the model field from the serializer's model
                model_field = self.Meta.model._meta.get_field(field_name)
                #print(f"Model field: {model_field}")
                
                # Check if it's a relation field (ForeignKey, OneToOne, etc.)
                if model_field.is_relation:
                    #print(f"Related model for {field_name}: {model_field.related_model}")
                    
                    # Get the related model (e.g., User for user field)
                    related_model = model_field.related_model
                    
                    # Try to find an appropriate serializer for this model
                    serializer_class = self._get_serializer_for_model(related_model)
                    #print(f"Found serializer class: {serializer_class}")
                    
                    if serializer_class:
                        # Convert the PrimaryKeyRelatedField to LeveledNestedField
                        #print(f"Converting {field_name} to LeveledNestedField")
                        fields[field_name] = LeveledNestedField(
                            serializer_class,
                            read_only=True
                        )
                        #print(f"New field type: {type(fields[field_name])}")
        
        #print(f"Final fields for {self.__class__.__name__}:", fields)
        return fields

    @classmethod
    def _get_serializer_for_model(cls, model):
        """
        Find appropriate serializer for given model by looking in the same module.
        
        Args:
            model: The model class to find a serializer for
            
        Returns:
            Serializer class or None
        """
        # Get the module where this serializer is defined
        module = sys.modules[cls.__module__]
        #print(f"Looking for serializer in module: {module.__name__}")
        
        # Look through all attributes in the module
        for attr_name in dir(module):
            attr = getattr(module, attr_name)
            #print(f"Checking attribute: {attr_name}, type: {type(attr)}")
            
            # Check if this attribute is a LeveledModelSerializer subclass
            if (isinstance(attr, type) and 
                issubclass(attr, LeveledModelSerializer) and 
                hasattr(attr, 'Meta') and 
                getattr(attr.Meta, 'model', None) == model):
                #print(f"Found matching serializer: {attr_name}")
                return attr
        
        #print(f"No existing serializer found for model: {model.__name__}")
        # Create dynamic serializer if none found
        dynamic_serializer = type(
            f'Dynamic{model.__name__}Serializer',
            (LeveledModelSerializer,),
            {
                'Meta': type('Meta', (), {
                    'model': model,
                    'fields': '__all__'
                })
            }
        )
        #print(f"Created dynamic serializer: {dynamic_serializer}")
        return dynamic_serializer

    def __init__(self, *args, **kwargs):
        """Initialize the serializer with level tracking"""
        # Get the nesting level from kwargs or context
        self.level = kwargs.pop('level', kwargs.get('context', {}).get('level', 1))
        #print(f"Initializing {self.__class__.__name__} with level: {self.level}")
        
        # Ensure context exists and contains level
        if 'context' not in kwargs:
            kwargs['context'] = {}
        kwargs['context']['level'] = self.level
        
        super().__init__(*args, **kwargs)


class LeveledNestedField(serializers.Field):
    """Field that adjusts serialization depth based on level"""

    def __init__(self, serializer_class, **kwargs):
        self.serializer_class = serializer_class
        self.level = kwargs.pop('level', None)
        super().__init__(**kwargs)

    def bind(self, field_name, parent):
        super().bind(field_name, parent)
        if self.level is None:
            parent_level = getattr(parent, 'level', 1)
            self.level = max(parent_level - 1, 0)

    def to_representation(self, value):
        if value is None:
            return None

        if self.level > 0:
            serializer = self.serializer_class(value,
                                               context={'level': self.level})
            return serializer.data
        else:
            # Return just the ID when we've reached max depth
            return value.pk
