from drf_spectacular.utils import extend_schema, extend_schema_view, OpenApiParameter, OpenApiTypes

generic_django_ops_docs = extend_schema_view(
    list=extend_schema(summary="List all objects",
                       description="Returns a paginated list of all available objects. Can be filtered using query parameters."),
    retrieve=extend_schema(summary="Get single object",
                           description="Retrieves a specific object by its unique identifier (ID)."),
    create=extend_schema(summary="Create new object",
                         description="Creates a new object with the provided data. Returns the created object."),
    update=extend_schema(summary="Update object",
                         description="Fully updates an existing object. All fields must be provided."),
    partial_update=extend_schema(summary="Partial update object",
                                description="Partially updates an existing object. Only specified fields will be modified."),
    destroy=extend_schema(summary="Delete object",
                          description="Permanently removes the specified object from the database."),
)

def term_schema_view():
    """Returns a reusable extend_schema_view decorator for views requiring term cookie"""
    return extend_schema(
                parameters=[
                    OpenApiParameter(
                        name='X-Term',
                        location=OpenApiParameter.HEADER,
                        description="Term provided in a header, e.g., 'term_WS_2024_2025'",
                        required=True,
                        default='term_WS_2024_2025',
                        type=OpenApiTypes.STR
                    ),
                ],
            )

