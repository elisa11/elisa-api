import re


def drf_spectacular_sort_tags_with_prio(result, generator, request, public):
    """
    Postprocess the OpenAPI schema to reorder and modify tags based on priority prefix `_int_`.
    Synchronize modified tags with those used in paths.
    """
    # Extract and collect all tags from paths
    tags_map = {}

    def tag_sort_key(tag):
        match = re.match(r'^_(\d+)_(.+)', tag)
        if match:
            priority = int(match.group(1))
            actual_name = match.group(2)
            return priority, actual_name
        return float('inf'), tag

    # Iterate over paths and update tags in each method
    for path, methods in result.get('paths', {}).items():
        for method, details in methods.items():
            if 'tags' in details:
                updated_tags = []
                for tag in details['tags']:
                    # Sort and clean tags
                    match = re.match(r'^_(\d+)_(.+)', tag)
                    if match:
                        clean_tag = match.group(2)
                        updated_tags.append(clean_tag)
                        tags_map[clean_tag] = tag_sort_key(tag)  # Store priority for sorting
                    else:
                        updated_tags.append(tag)
                        tags_map[tag] = tag_sort_key(tag)
                # Update tags for the method
                details['tags'] = updated_tags

    # Sort and deduplicate tags based on the collected map
    sorted_tags = sorted(tags_map.keys(), key=lambda tag: tags_map[tag])
    result['tags'] = [{"name": tag} for tag in sorted_tags]

    return result



