from django.http import JsonResponse
from rest_framework import serializers
from datetime import datetime

from rest_framework.response import Response

class Err_serializer(serializers.Serializer):
    code = serializers.IntegerField()
    message = serializers.CharField()
    timestamp = serializers.DateTimeField(default=datetime.now().isoformat())


class Ok_serializer(serializers.Serializer):
    code = serializers.IntegerField()
    message = serializers.CharField()
    timestamp = serializers.DateTimeField(default=datetime.now().isoformat())

def ok_response(message: str="ok", status_code: int=200):
    return Response(
        {
            "code": status_code,
            "message": message,
            "timestamp": datetime.now().isoformat()
        },
        status=status_code
    )


def err_response(message: str, status_code: int=400):
    return JsonResponse(
        {
            "code": status_code,
            "message": message,
            "timestamp": datetime.now().isoformat()
        },
        status=status_code
    )